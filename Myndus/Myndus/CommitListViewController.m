//
//  CommitListViewController.m
//  Myndus
//
//  Created by Troy DeMar on 10/7/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "CommitListViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "UserDefaultsStore.h"

@interface CommitListViewController ()

@end

@implementation CommitListViewController
@synthesize dataTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Data Table
    self.dataTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}


- (void)vibrate{
    AudioServicesPlaySystemSound (1352);
}


- (NSAttributedString *)attribTextForComment:(NSString *)aComment{
    NSMutableParagraphStyle *paragraphStyle=[[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    
    NSAttributedString *attribText = [[NSAttributedString alloc] initWithString:aComment
                                                                     attributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:14.0], NSParagraphStyleAttributeName: paragraphStyle}];
    
    return attribText;
}

- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width {
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setAttributedText:text];
    CGSize size = [calculationView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    
    
    //NSLog(@"Text: %@; Height: %f", text.string, size.height);
    return size.height;
}




#pragma mark - IBAction

- (IBAction)clostAction:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    // Return the number of rows in the section.
    NSArray *commitsArray = [[UserDefaultsStore shared] chosenCommitmentArray];
    NSLog(@"Commits: %i", commitsArray.count);
    
    return commitsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int row = [indexPath row];
    static NSString *cellIdentifier = @"CommitCell";
    
    CommitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // Configure the cell...
    cell.delegate = self;
    [cell setIndex:row];
    cell.contentView.alpha=1.0f;
    cell.checkBtn.hidden = YES;
    cell.userInteractionEnabled = YES;
    
    NSArray *commitsArray = [[UserDefaultsStore shared] chosenCommitmentArray];
    NSDictionary *dict = [commitsArray objectAtIndex:row];
    NSString *sentence = [dict objectForKey:@"sentence"];
    NSNumber *imageNumber = [dict objectForKey:@"imageNumber"];
    
    
    
    cell.textView.text = sentence;

    //Hack Image
    //NSInteger random = 1 + arc4random_uniform(2 - 1 + 1);
    if ([imageNumber intValue] == 0)
        cell.imgView.image = [UIImage imageNamed:@"Hack_Commitment_Shapes_triangleA"];
    else if ([imageNumber intValue] == 1)
        cell.imgView.image = [UIImage imageNamed:@"Hack_Commitment_Shapes_triangleB"];
    else if ([imageNumber intValue] == 2)
        cell.imgView.image = [UIImage imageNamed:@"Hack_Commitment_Shapes_squareA.png"];
    else if ([imageNumber intValue] == 3)
        cell.imgView.image = [UIImage imageNamed:@"Hack_Commitment_Shapes_squareB.png"];
    
    
    //Checkmark Image
    if ([cell isCommitmentCheckedToday]) {
        cell.imgView.alpha = 0.5f;
        cell.textView.alpha = 0.5f;
        [cell.checkBtn setBackgroundImage:[UIImage imageNamed:@"CheckMark.png"] forState:UIControlStateNormal];
        cell.checkBtn.hidden = NO;
        //cell.userInteractionEnabled = NO;
    }
    else {
        cell.imgView.alpha = 1.0f;
        cell.textView.alpha = 1.0f;
        [cell.checkBtn setBackgroundImage:[UIImage imageNamed:@"Checkbox.png"] forState:UIControlStateNormal];
        cell.userInteractionEnabled = YES;
    }

    
    return cell;
}



#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    int row = [indexPath row];
    
    NSArray *commitsArray = [[UserDefaultsStore shared] chosenCommitmentArray];
    NSDictionary *dict = [commitsArray objectAtIndex:row];
    NSString *sentence = [dict objectForKey:@"sentence"];;
    
    
    //NSLog(@"Height: %f", [self textViewHeightForAttributedText:[self attribTextForComment:sentence] andWidth:254]);
    
    if ([self textViewHeightForAttributedText:[self attribTextForComment:sentence] andWidth:254] < 30)
        return 30;
    
    return [self textViewHeightForAttributedText:[self attribTextForComment:sentence] andWidth:254];
    
}




#pragma mark - StartVCCommitCellDelegate

- (void)commitCell:(CommitTableViewCell *)cell didTapCellButton:(UIButton *)button atIndex:(int)index{
    //NSLog(@"CommitCell Touched");
    
//    if (selectedStartCommitCell != nil )    //&& selectedStartCommitCell != cell
//        [selectedStartCommitCell displayCurrent];
//

    if (selectedCommitCell != cell){
        //NSLog(@"Not same cell");
        [selectedCommitCell displayCurrent];
        
        selectedCommitCell = cell;
    }
    
    else if (selectedCommitCell == cell && [cell isCommitmentCheckedToday]) {
        [selectedCommitCell displayCurrent];
        
        selectedCommitCell = nil;
    }
    
    
    
    
    //NSLog(@"All Completed Today: %@", [[UserDefaultsStore shared] commitmentsCompletedToday] ? @"YES" : @"NO");
    
}


- (void)commitCell:(CommitTableViewCell *)cell didTapCheckButton:(UIButton *)button atIndex:(int)index{
    //NSLog(@"Box/Check Touched");
    selectedCommitCell = nil;
    [self vibrate];
    
//    NSLog(@"All Completed Today: %@", [[UserDefaultsStore shared] commitmentsCompletedToday] ? @"YES" : @"NO");
//    if ([[UserDefaultsStore shared] commitmentsCompletedToday]) {
//        [[UserDefaultsStore shared] addProgressStarToday];
//    }
//    else {
//        [[UserDefaultsStore shared] removeProgressStarToday];
//    }
//    
//    
//    //Update Star Progress Bar
//    [self updateProgressStar];
}





@end
