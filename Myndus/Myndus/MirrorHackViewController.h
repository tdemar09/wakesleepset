//
//  MirrorHackViewController.h
//  Myndus
//
//  Created by Troy DeMar on 10/1/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HackBaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SendMail.h"


@interface MirrorHackViewController : HackBaseViewController <SKPSMTPMessageDelegate> {
    IBOutlet UIView *waitView, *sliderView, *sliderBarView, *zeroLineView;
    IBOutlet UIView *cameraView;
    IBOutlet UIButton *sliderBtn, *contineBtn, *exitBtn;
    IBOutlet UILabel *label;
    IBOutlet UILabel *leftLbl, *rightLbl, *zeroLbl;
    IBOutlet UISlider *slider;
    
    AVCaptureStillImageOutput *stillImageOutput;
    AVCaptureDevice *camera;

    NSTimer *timer_instr, *timer_dec, *timer_min, *secsTimer;

    CGFloat fps;
    int fps_end, secs;
}

@property (strong, nonatomic) UIImageView *imgView;
@property (nonatomic, strong) UIView *waitView, *sliderView, *sliderBarView, *zeroLineView;
@property (strong, nonatomic) UIView *cameraView;
@property (strong, nonatomic) UIButton *sliderBtn, *instrBtn, *contineBtn, *exitBtn;
@property (strong, nonatomic) UILabel *label;
@property (nonatomic, strong) UILabel *leftLbl, *rightLbl, *zeroLbl;
@property (nonatomic, strong) UISlider *slider;


@end
