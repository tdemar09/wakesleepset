//
//  RotateHackViewController.m
//  Myndus
//
//  Created by Troy DeMar on 10/2/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "RotateHackViewController.h"

@interface RotateHackViewController ()
@property (nonatomic) BOOL complete;
@end

@implementation RotateHackViewController
@synthesize imgView;
@synthesize count;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDidRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    self.instrView.hidden = YES;
    self.navView.hidden = YES;
    
    NSLog(@"Count: %i", self.count);

}



- (void)deviceDidRotate:(NSNotification *)notification{
    UIDeviceOrientation currentOrientation = [[UIDevice currentDevice] orientation];
    double rotation = 0;
    UIInterfaceOrientation statusBarOrientation;
    switch (currentOrientation) {
        case UIDeviceOrientationLandscapeLeft:
            NSLog(@"Left");
            statusBarOrientation = UIInterfaceOrientationLandscapeRight;
            rotation = M_PI_2;
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
            [self continueOn];
            break;
            
        case UIDeviceOrientationLandscapeRight:
            NSLog(@"Right");
            rotation = -M_PI_2;
            return;
            
            
        case UIDeviceOrientationPortrait:
            rotation = 0;
            statusBarOrientation = UIInterfaceOrientationPortrait;
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            rotation = -M_PI;
            statusBarOrientation = UIInterfaceOrientationPortraitUpsideDown;
            break;
            
            
        default:
            return;
    }

//    CGAffineTransform transform = CGAffineTransformMakeRotation(rotation);
//    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
//        [self.imgView setTransform:transform];
//        [[UIApplication sharedApplication] setStatusBarOrientation:statusBarOrientation];
//    } completion:nil];
}





#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
    
    
    if (!self.complete)
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight);
    
    else return UIInterfaceOrientationMaskPortrait;
    
//    return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeRight);

}

-(BOOL)shouldAutorotate{
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    NSLog(@"willRotateToInterfaceOrientation");
    
    imgView.hidden = YES;
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait) {
        NSLog(@"Portrait");
        
        //imgView.image = [UIImage imageNamed:@"RotateTask3.png"];
        
    }
    
    else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        NSLog(@"Left");
    }
    
    else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        NSLog(@"Right");
        
        self.complete = YES;
        
        //imgView.image = [UIImage imageNamed:@"RotateTaskA2.png"];
    }
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    if (self.complete) {
        
        [self performSelector:@selector(continueOn) withObject:nil afterDelay:0.5];
    }
    
}



- (void)continueOn{
    //NSLog(@"continueOn");
    
    if (self.sequence == HackSequenceNew) {
        //NSLog(@"Count: %i", self.count);
        
        if (self.count == 1)
            [self performSegueWithIdentifier:@"karolinaksaSegue" sender:nil];
        
        else if (self.count == 2) {
            //Random Commit or Highlight
            NSInteger min = 1;
            NSInteger max = 3;
            NSInteger random = min + arc4random_uniform(max - min + 1);
            //NSLog(@"Random: %i", random);
            if (random == 1) {
                //Commitment
                [self performSegueWithIdentifier:@"SeqNew_RandomCommitSegue" sender:nil];
                
            }
            else if (random == 2) {
                //Highlighter
                [self performSegueWithIdentifier:@"SeqNew_RandomHighlightSegue" sender:nil];
                
            }
            
            else {
                [self performSegueWithIdentifier:@"SeqNew_RandomPersonalitySegue" sender:nil];
                
            }
            
        }
        
    }
}


- (void)continueOnOLD{
    UIApplication* application = [UIApplication sharedApplication];
    if (application.statusBarOrientation != UIInterfaceOrientationPortrait) {
        UIViewController *c = [[UIViewController alloc]init];
        //[c.view setBackgroundColor:[UIColor redColor]];
        
        [self.navigationController presentViewController:c animated:NO completion:^{
            [self.navigationController dismissViewControllerAnimated:NO completion:^{
                
                
                //Continue On
                
                if (self.sequence == HackSequenceNew) {
                    
                    if (self.count == 1)
                        [self performSegueWithIdentifier:@"karolinaksaSegue" sender:nil];
                    
                    else if (self.count == 2) {
                        //Random Commit or Highlight
                        NSInteger min = 1;
                        NSInteger max = 3;
                        NSInteger random = min + arc4random_uniform(max - min + 1);
                        //NSLog(@"Random: %i", random);
                        if (random == 1) {
                            //Commitment
                            [self performSegueWithIdentifier:@"SeqNew_RandomCommitSegue" sender:nil];
                        
                        }
                        else if (random == 2) {
                            //Highlighter
                            [self performSegueWithIdentifier:@"SeqNew_RandomHighlightSegue" sender:nil];

                        }
                        
                        else {
                            [self performSegueWithIdentifier:@"SeqNew_RandomPersonalitySegue" sender:nil];
                            
                        }
                        
                    }
                    
                }
                
            }];
        }];
    }
    
    
    
    

}




@end
