//
//  HackPersonalityViewController.m
//  Mynd
//
//  Created by Sanjay on 29/07/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "HackPersonalityViewController.h"
#import "UserDefaultsStore.h"



@interface HackPersonalityViewController ()

@end

@implementation HackPersonalityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    instrImageView.image = [UIImage imageNamed:@"HackIntro_Personality.png"];

    if (![[UserDefaultsStore shared] isPersonalityInstrDone]) {
        [[UserDefaultsStore shared] setPersonalityInstrDone:YES];
    }else {
        instrView.hidden = YES;
    }
    
    
//    NSArray *arr_Of_Questions=[[NSArray alloc]initWithObjects:@"You are almost never late for your appointments",@"You like to be engaged in an active and fast-paced job",@"You enjoy having a wide circle of acquaintances",@"You feel involved when watching TV shows",@"You are more interested in a general idea than in the details of its realization",@"You tend to be unbiased even if this might endanger your good relations with people",@"Strict observance of the established rules is likely to prevent a good outcome",@"It's difficult to get you excited",@"It is in your nature to assume responsibility",@"You often think about humankind and its destiny",@"You believe the best decision is one that can be easily changed",@"Objective criticism is always useful in any activity",@"You prefer to act immediately rather than speculate about various options",@"You trust reason rather than feelings",@"You are inclined to rely more on improvisation than on prior planning",@"You spend your leisure time actively socializing with a group of people, attending parties, shopping, etc.",@"You feel involved when watching TV shows",@"You usually plan your actions in advance",@"Your actions are frequently influenced by emotions",@"You are a person somewhat reserved and distant in communication",@"You know how to put every minute of your time to good purpose",@"You readily help people while asking nothing in return",@"You often contemplate the complexity of life",@"After prolonged socializing you feel you need to get away and be alone",@"You often do jobs in a hurry",@"You easily see the general principle behind specific occurrences",@"You frequently and easily express your feelings and emotions",@"You find it difficult to speak loudly",@"You get bored if you have to read theoretical books",@"You tend to sympathize with other people",@"You value justice higher than mercy",@"You rapidly get involved in the social life of a new workplace",@"The more people with whom you speak, the better you feel",@"You tend to rely on your experience rather than on theoretical alternatives",@"As a rule, you proceed only when you have a clear and detailed plan",@"You easily empathize with the concerns of other people",@"You often prefer to read a book than go to a party",@"You enjoy being at the center of events in which other people are directly involved",@"You are more inclined to experiment than to follow familiar approaches",@"You avoid being bound by obligations",@"You are strongly touched by stories about people's troubles",@"Deadlines seem to you to be of relative, rather than absolute, importance",@"You prefer to isolate yourself from outside noises",@"It's essential for you to try things with your own hands",@"You think that almost everything can be analyzed",@"For you, no surprises is better than surprises - bad or good ones",@"You take pleasure in putting things in order",@"You feel at ease in a crowd",@"You have good control over your desires and temptations",@"You easily understand new theoretical principles",@"The process of searching for a solution is more important to you than the solution itself",@"You usually place yourself nearer to the side",@"than in the center of a room",@"When solving a problem you would rather follow a familiar approach than seek a new one",@"You try to stand firmly by your principles",@"A thirst for adventure is close to your heart",@"You prefer meeting in small groups over interaction with lots of people",@"When considering a situation you pay more attention to the current situation and less to a possible sequence of events",@"When solving a problem you consider the rational approach to be the best",@"You find it difficult to talk about your feelings",@"You often spend time thinking of how things could be improved",@"Your decisions are based more on the feelings of a moment than on the thorough planning",@"You prefer to spend your leisure time alone or relaxing in a tranquil atmosphere",@"You feel more comfortable sticking to conventional ways",@"You are easily affected by strong emotions",@"You are always looking for opportunities",@"Your desk, workbench, etc. is usually neat and orderly",@"As a rule, current preoccupations worry you more than your future plans",@"You get pleasure from solitary walks",@"It is easy for you to communicate in social situations",@"You are consistent in your habits",@"You willingly involve yourself in matters which engage your sympathies",@"You easily perceive various ways in which events could develop", nil];
    
    NSArray *arr_Of_Questions = [[UserDefaultsStore shared] personalityQuestions];
    
    int NoOfCount=[[[NSUserDefaults standardUserDefaults]valueForKey:kPersonalityCountKey]intValue];
    NSLog(@"kPersonalityCountKey: %i", NoOfCount);
    
    
//    NoOfCount=NoOfCount*10;
//    
    if(NoOfCount+4>=arr_Of_Questions.count){
        int i=0;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:kPersonalityCountKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NoOfCount=0;
    }else{
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",NoOfCount+5] forKey:kPersonalityCountKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    NSLog(@"NoOfCount: %i", NoOfCount);
    NSLog(@"Text1: %@", [arr_Of_Questions objectAtIndex:0]);
    
    lbl_PersonQ1.text=[arr_Of_Questions objectAtIndex:NoOfCount];
    lbl_PersonQ2.text=[arr_Of_Questions objectAtIndex:NoOfCount+1];
    lbl_PersonQ3.text=[arr_Of_Questions objectAtIndex:NoOfCount+2];
    lbl_PersonQ4.text=[arr_Of_Questions objectAtIndex:NoOfCount+3];
    lbl_PersonQ5.text=[arr_Of_Questions objectAtIndex:NoOfCount+4];
    
//    arrSecondFive=[[NSMutableArray alloc]initWithObjects:[arr_Of_Questions objectAtIndex:NoOfCount+5],[arr_Of_Questions objectAtIndex:NoOfCount+6],[arr_Of_Questions objectAtIndex:NoOfCount+7],[arr_Of_Questions objectAtIndex:NoOfCount+8],[arr_Of_Questions objectAtIndex:NoOfCount+9], nil];
    
    btn_Continue.enabled=NO;
    
    //Set the Count at +5

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Click Event
- (IBAction)ContinueClicked:(id)sender
{
    if (![[NSUserDefaults standardUserDefaults]valueForKey:@"TotalPlayedCount"]) {
        int i=1;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:@"TotalPlayedCount"];
    }
    else
    {
        int i=[[[NSUserDefaults standardUserDefaults]valueForKey:@"TotalPlayedCount"] intValue];
        i++;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:@"TotalPlayedCount"];
    }
}
- (IBAction)ClickEvent1Yes:(id)sender
{
    [self MakeITSelectedYes:btn_1_Yes MakeNO:btn_1_No];
    btn_1_No.backgroundColor=[UIColor clearColor];
    btn_1_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent2Yes:(id)sender
{
    [self MakeITSelectedYes:btn_2_Yes MakeNO:btn_2_No];
    btn_2_No.backgroundColor=[UIColor clearColor];
    btn_2_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];

}
- (IBAction)ClickEvent3Yes:(id)sender
{
    [self MakeITSelectedYes:btn_3_Yes MakeNO:btn_3_No];
    btn_3_No.backgroundColor=[UIColor clearColor];
    btn_3_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];

}
- (IBAction)ClickEvent4Yes:(id)sender
{
    [self MakeITSelectedYes:btn_4_Yes MakeNO:btn_4_No];
    btn_4_No.backgroundColor=[UIColor clearColor];
    btn_4_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];

}
- (IBAction)ClickEvent5Yes:(id)sender
{
    [self MakeITSelectedYes:btn_5_Yes MakeNO:btn_5_No];
    btn_5_No.backgroundColor=[UIColor clearColor];
    btn_5_Yes.backgroundColor=[UIColor colorWithRed:0 green:25 blue:0 alpha:0.5];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent1No:(id)sender
{
    [self MakeITSelectedYes:btn_1_No MakeNO:btn_1_Yes];
    btn_1_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_1_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent2No:(id)sender
{
    [self MakeITSelectedYes:btn_2_No MakeNO:btn_2_Yes];
    btn_2_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_2_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent3No:(id)sender
{
    [self MakeITSelectedYes:btn_3_No MakeNO:btn_3_Yes];
    btn_3_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_3_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent4No:(id)sender
{
    [self MakeITSelectedYes:btn_4_No MakeNO:btn_4_Yes];
    btn_4_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_4_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
- (IBAction)ClickEvent5No:(id)sender
{
    [self MakeITSelectedYes:btn_5_No MakeNO:btn_5_Yes];
    btn_5_No.backgroundColor=[UIColor colorWithRed:25 green:0 blue:0 alpha:0.5];
    btn_5_Yes.backgroundColor=[UIColor clearColor];
    [self CheckForTheCondition];
}
-(void)MakeITSelectedYes:(UIButton *)btn1 MakeNO:(UIButton *)btn2
{
    btn1.selected=YES;
    btn2.selected=NO;
}
-(void)CheckForTheCondition{
    if (btn_1_Yes.selected||btn_1_No.selected)
    {
        
    }
    else
        return;
    if (btn_2_Yes.selected||btn_2_No.selected)
    {
        
    }
    else
        return;
    
    if (btn_3_Yes.selected||btn_3_No.selected)
    {
        
    }
    else
        return;
    
    if (btn_4_Yes.selected||btn_4_No.selected)
    {
        
    }
    else
        return;
    
    if (btn_5_Yes.selected||btn_5_No.selected)
    {
        
    }
    else
        return;

    btn_Continue.enabled=YES;
    if (![[NSUserDefaults standardUserDefaults]valueForKey:@"TotalPlayedCount"]) {
        int i=1;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:@"TotalPlayedCount"];
    }
    else
    {
        int i=[[[NSUserDefaults standardUserDefaults]valueForKey:@"TotalPlayedCount"] intValue];
        i++;
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",i] forKey:@"TotalPlayedCount"];
    }
    
    
    //[self performSegueWithIdentifier:@"Hack2View" sender:self];
    
    if (!self.repeatSeq) {
        [self performSegueWithIdentifier:@"SeqNew_HighlightStartSegue" sender:self];
    }
    else {
        NSLog(@"Repeat");
        [self repeatSequence:nil];
    }
}




// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Hack2View"])
    {
//        HackPersonality2ViewController *controller=(HackPersonality2ViewController *)[segue destinationViewController];
//        controller.arr_SecondFiveQ=arrSecondFive;
    }
}

#pragma mark - Navigation Delegate

- (IBAction)ClickToRoot:(id)sender
{
    
    self.navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self dismissViewControllerAnimated:YES completion:nil];

}
@end
