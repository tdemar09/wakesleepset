//
//  CommitScoreViewController.h
//  Mynd
//
//  Created by Troy DeMar on 7/23/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommitScoreViewController : UIViewController{
    IBOutlet UIImageView *imageView;
    IBOutlet UILabel *todayLbl, *totalLbl;
}

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *todayLbl, *totalLbl;

@property (nonatomic, strong) NSString *answerSentence;
@property (nonatomic) NSInteger answerOption;

@end
