//
//  AppDelegate.h
//  Myndus
//
//  Created by Troy DeMar on 9/30/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UISlider *slider;

@property (nonatomic, strong) NSNumber *sliderNumber;
@property (nonatomic) NSInteger snakeAccuracy, snakeTailMoves, snakeTouches;


- (UISlider *)mainSlider;
- (UISlider *)newSlider;
- (void)presentSequence:(HackSequence)sequence;

@end
