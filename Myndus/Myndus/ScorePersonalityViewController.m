//
//  ScorePersonalityViewController.m
//  Mynd
//
//  Created by Sanjay on 29/07/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "ScorePersonalityViewController.h"
#import "UserDefaultsStore.h"

@interface ScorePersonalityViewController ()

@end

@implementation ScorePersonalityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;

    
     // Do any additional setup after loading the view.
    
    [[UserDefaultsStore shared] personalityHackDoneToday];
}

-(void)viewWillAppear:(BOOL)animated
{
    
}
- (void)setupScore{
    //generate today score
    NSInteger score = [[UserDefaultsStore shared] generateHighlighterScore];
    //save to total
    [[UserDefaultsStore shared] incrementHighlighterScore:score];
   
}
-(void)PreViousMethodToSetupScore
{
    int noOfCount=[[[NSUserDefaults standardUserDefaults] valueForKey:@"TotalPlayedCount"] intValue];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBAction Methods
-(IBAction)EyeClick:(id)sender
{
    self.navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)ClickedContinue:(id)sender
{
    self.navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
