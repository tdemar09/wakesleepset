//
//  UserDefaultsStore.m
//  Mynd
//
//  Created by Shuaib on 01/07/2014.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "UserDefaultsStore.h"
#import "NSDate-Utilities.h"
#import "SendMail.h"

@interface UserDefaultsStore ()

@property (nonatomic, strong) NSMutableArray *unAnsweredHackSentences;  //, *unAskedHighlightedHackSentences;
@property (nonatomic, strong) NSMutableDictionary *answeredHackSentences;

@end

@implementation UserDefaultsStore

+ (UserDefaultsStore *)shared
{
    static UserDefaultsStore *userDefaultsStore = nil;
    if (!userDefaultsStore) {
        userDefaultsStore = [UserDefaultsStore new];
        [userDefaultsStore setUp];
    }
    return userDefaultsStore;
}

#pragma mark - Instance Methods

- (NSString *)unAnsweredHackSentence
{
    NSInteger index = arc4random() % self.unAnsweredHackSentences.count;
    return self.unAnsweredHackSentences[index];
}

- (void)saveAnswer:(BOOL)answer forHackSenctence:(NSString *)sentence
{
    self.answeredHackSentences[sentence] = @(answer);
    [self.unAnsweredHackSentences removeObject:sentence];
}


- (NSDate *)firstLaunch{
    NSDate *first = [[NSUserDefaults standardUserDefaults] objectForKey:@"firstLaunch"];
    return first;
}

- (NSString *)username{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Username"];
}

- (void)setUsername:(NSString *)username{
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"Username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




#pragma Current Week

- (void)setToday{
    NSDate *today = [NSDate date];
    
    [[NSUserDefaults standardUserDefaults] setObject:today forKey:@"today"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (BOOL)isToday{
    NSDate *today = [NSDate date];
    NSDate *savedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"today"];
    if (!savedDate)
        return NO;
    
    
    NSDateComponents *dayComps = [[NSDateComponents alloc] init];
    [dayComps setMonth:[today month]];
    [dayComps setDay:[today day]];
    [dayComps setYear:[today year]];
    [dayComps setHour:0];
    [dayComps setMinute:1];
    today = [[NSCalendar currentCalendar] dateFromComponents:dayComps];
    
    
    [dayComps setMonth:[savedDate month]];
    [dayComps setDay:[savedDate day]];
    [dayComps setYear:[savedDate year]];
    [dayComps setHour:0];
    [dayComps setMinute:1];
    savedDate = [[NSCalendar currentCalendar] dateFromComponents:dayComps];
    
    if ([today isLaterThanDate:savedDate])
        return NO;
    
    return YES;
}



- (NSNumber *)currentWeekDayCount{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentWeek"];
    
    return [dict objectForKey:@"dayCount"];
}

- (BOOL)isCurrentWeekDayComplete{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentWeek"];
    NSNumber *complete = [dict objectForKey:@"dayComplete"];
    
    return [complete boolValue];
}

- (void)setCurrentWeekDayComplete:(BOOL)complete{
    NSMutableDictionary *dict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentWeek"] mutableCopy];
    [dict setObject:[NSNumber numberWithBool:complete] forKey:@"dayComplete"];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"currentWeek"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"Set Day Complete!!");
}



- (NSNumber *)currentWeekHackSequenceCountForExperiment:(Experiment)exp{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentWeek"];
    NSString *expString = [NSString stringWithFormat:@"experiment%i", exp];
    NSDictionary *expDict = [dict objectForKey:expString];

    return [expDict objectForKey:@"hackSequenceCount"];
}

- (HackSequence)currentWeekHackSequenceForExperiment:(Experiment)exp{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentWeek"];
    NSString *expString = [NSString stringWithFormat:@"experiment%i", exp];
    NSDictionary *expDict = [dict objectForKey:expString];
    
    NSNumber *num = [expDict objectForKey:@"hackSequence"];
    
    return [num intValue];
}

- (void)setCurrentWeekHackSequence:(HackSequence)sequence forExperiment:(Experiment)exp count:(int)count{
    
    //add Bool Started
    NSMutableDictionary *dict = [[[NSUserDefaults standardUserDefaults] objectForKey:@"currentWeek"] mutableCopy];
    NSString *expString = [NSString stringWithFormat:@"experiment%i", exp];
    NSMutableDictionary *expDict = [[dict objectForKey:expString] mutableCopy];
    
    [expDict setObject:[NSNumber numberWithInt:sequence] forKey:@"hackSequence"];
    [expDict setObject:[NSNumber numberWithInt:count] forKey:@"hackSequenceCount"];
    [dict setObject:expDict forKey:expString];
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"currentWeek"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




#pragma Emails
- (void)addEmail:(NSString *)email{
    NSMutableArray *array = [[[NSUserDefaults standardUserDefaults] objectForKey:@"emailSave"] mutableCopy];
    if (!array) {
        array = [[NSMutableArray alloc] init];
    }
    
    [array addObject:email];
    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"emailSave"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)sendEmails{
    NSMutableArray *array = [[[NSUserDefaults standardUserDefaults] objectForKey:@"emailSave"] mutableCopy];
    if (array.count > 0) {
        for (NSString *email in array) {
            SendMail *sendMail = [SendMail new];
            [sendMail sendMessageInBack:email];
            NSLog(@"attempting to send email: %@", email);
        }
        
        [array removeAllObjects];
        
        [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"emailSave"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}



#pragma KarolinskaScale





#pragma PrescribedSleepTimer (TurboSleep)
- (BOOL)isPrescribedSleepTimerDone{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"prescribedSleepTimerDate"];
    if (saveDate)
        return YES;
    
    
    return NO;
}

- (BOOL)shouldDisplayPrescribedSleep{
    NSDate *begdt = [self firstLaunch];
    NSDate *weekdt = [begdt dateByAddingDays:6];
    
    if (!begdt || !weekdt)
        return NO;
    
    
    //Check Today is >= Begdt + 7
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *weekDateComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:weekdt];
    
    
    
    if ([[cal dateFromComponents:todayComp] isLaterThanDate:[cal dateFromComponents:weekDateComp]])
        return YES;
    
    
    return NO;
}

- (NSDate *)prescribedInBedDate{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"prescribedInBedDate"];
}

- (NSDate *)prescribedOutBedDate{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"prescribedOutBedDate"];
}


- (void)setPrescribedSleepTime:(NSDate *)inBedDate wakeDate:(NSDate *)outBedDate{
    [[NSUserDefaults standardUserDefaults] setObject:inBedDate forKey:@"prescribedInBedDate"];
    [[NSUserDefaults standardUserDefaults] setObject:outBedDate forKey:@"prescribedOutBedDate"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"prescribedSleepTimerDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)prescribedSleepTimerDone{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"prescribedSleepTimerDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)forceDisplayPrescribedSleep{
    NSDate *date = [[NSDate date] dateBySubtractingDays:8];
    
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"firstLaunch"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"prescribedSleepTimerDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




#pragma mark - Instance Methods Hacks

#pragma Mirror

- (BOOL)isMirrorInstrDone{
    NSNumber *boolNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"mirrorInstrDone"];
    return [boolNumber boolValue];
}

- (void)setMirrorInstrDone:(BOOL)done{
    NSNumber *boolNumber = [NSNumber numberWithBool:done];
    
    [[NSUserDefaults standardUserDefaults] setObject:boolNumber forKey:@"mirrorInstrDone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma Highlighter
- (BOOL)isHighlighterInstrDone{
    NSNumber *boolNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"highlighterInstrDone"];
    return [boolNumber boolValue];
}

- (void)setHighlighterInstrDone:(BOOL)done{
    NSNumber *boolNumber = [NSNumber numberWithBool:done];
    
    [[NSUserDefaults standardUserDefaults] setObject:boolNumber forKey:@"highlighterInstrDone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)unAskedHighlighterSentence{
    //return [self.unAskedHighlightedHackSentences firstObject];
    
    NSArray *array = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"unAskedHighlightedHackSentences"] mutableCopy];
    if (!array || array.count == 0) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"HighlighterHack" ofType:@"plist"];
        array = [NSArray arrayWithContentsOfFile:path];
        
        return [array firstObject];
    }
    
    if ([array count] > 0)
        return [array firstObject];
    
    return nil;
}


- (NSInteger)generateHighlighterScore{
    NSInteger min = 160;
    NSInteger max = 320;
    
    return min + arc4random_uniform(max - min + 1);
}

- (NSInteger)highlighterTotal{
    NSNumber *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"highlighterScore"];

    if (!temp)
        return 0;
    
    return temp.integerValue;
}

- (BOOL)isHighlighterDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"highlighterLastDoneDate"];
    if (!saveDate)
        return NO;
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];

    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}


- (void)highlighterHackChoseWords:(NSArray *)wordsArray{
    NSMutableArray *array = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"unAskedHighlightedHackSentences"] mutableCopy];
    if (!array || array.count == 0) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"HighlighterHack" ofType:@"plist"];
        array = [NSMutableArray arrayWithContentsOfFile:path];
    }
    
    if (array.count > 0) {
        //Create Used Entry Dict & Save in Used Array
        NSString *sentence = [array objectAtIndex:0];
        NSMutableDictionary *entry = [[NSMutableDictionary alloc] initWithObjectsAndKeys:sentence, @"sentence", wordsArray, @"words", nil];
        
        NSMutableArray *usedArray = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"askedHighlightedHackSentences"] mutableCopy];
        if (!usedArray)
            usedArray = [NSMutableArray array];
        
        [usedArray addObject:entry];
        [[NSUserDefaults standardUserDefaults] setObject:usedArray forKey:@"askedHighlightedHackSentences"];

        
        
        //Remove used sentence and Save with Date
        [array removeObjectAtIndex:0];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"unAskedHighlightedHackSentences"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"highlighterLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)incrementHighlighterScore:(NSInteger)score{
    NSNumber *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"highlighterScore"];
    if (!temp) {
        temp = [NSNumber numberWithInt:0];
    }
    
    NSNumber *total = [NSNumber numberWithInt:temp.integerValue + score];
    [[NSUserDefaults standardUserDefaults] setObject:total forKey:@"highlighterScore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma Highlighter2

- (BOOL)isHighlighter2DoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"highlighter2LastDoneDate"];
    if (!saveDate)
        return NO;
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}

- (void)highlighter2Done{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"highlighter2LastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}





#pragma Commitment

- (BOOL)isCommitmentInstrDone{
    NSNumber *boolNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"commentmentInstrDone"];
    return [boolNumber boolValue];
}

- (void)setCommitmentInstrDone:(BOOL)done{
    NSNumber *boolNumber = [NSNumber numberWithBool:done];
    
    [[NSUserDefaults standardUserDefaults] setObject:boolNumber forKey:@"commentmentInstrDone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasUnUsedCommitmentDictionary{
    NSArray *array = [[NSUserDefaults standardUserDefaults] arrayForKey:@"unUsedCommitmentDictionary"];
    if (!array) return YES;
    if (array.count > 0) return YES;
    else return NO;
}

- (NSDictionary *)unUsedCommitmentDictionary
{
    NSArray *array = [[NSUserDefaults standardUserDefaults] arrayForKey:@"unUsedCommitmentDictionary"];
    if (!array || array.count == 0) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CommitmentHack" ofType:@"plist"];
        array = [NSArray arrayWithContentsOfFile:path];
        return [array firstObject];
    }
    
    if ([array count] > 0)
        return [array firstObject];
    
    return nil;
}

- (NSArray *)chosenCommitmentArray{
    NSArray *array = [[NSUserDefaults standardUserDefaults] arrayForKey:@"chosenCommitmentArray"];
    if (!array) {
        array = [[NSMutableArray alloc] init];
    }
    
    return array;
}


- (NSArray *)commitmentPercentagesArray{
    NSInteger opt1Min = 3, opt1Max = 17;
    //NSInteger opt2Min = 66, opt2Max = 95;
    NSInteger opt3Min = 3, opt3Max = 17;

    //Find Option 1 %
    NSInteger perc1 = opt1Min + arc4random_uniform(opt1Max - opt1Min + 1);
    
    //Find Option 3 %
    NSInteger perc3 = opt3Min + arc4random_uniform(opt3Max - opt3Min + 1);
    
    //Use 1 & 3 to get 2 (total 100)
    NSInteger perc2 = 100 - perc1 - perc3;
    
    return [NSArray arrayWithObjects:[NSNumber numberWithInteger:perc1], [NSNumber numberWithInteger:perc2], [NSNumber numberWithInteger:perc3], nil];
}

- (BOOL)isCommitmentPlusOnTop{
    NSInteger min = 1;
    NSInteger max = 3;
    
    NSInteger result = min + arc4random_uniform(max - min + 1);
    
    if (result == 1 || result == 2)
        return YES;
    
    return NO;
}

- (UIImage *)commitmentSigImage{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"CommitSignature.jpg"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];

    if (fileExists) {
        return [UIImage imageWithContentsOfFile:filePath];
    }
    
    return nil;
}

- (BOOL)haveCommitmentSignature{
    /*
    NSData *sigData = [[NSUserDefaults standardUserDefaults] objectForKey:@"highlighterSignature"];
    if (!sigData)
        return NO;
    */
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"CommitSignature.jpg"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];

    if (!fileExists) {
        return NO;
    }
    
    return YES;
}

- (BOOL)isCommitmentDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"commitmentLastDoneDate"];
    if (!saveDate)
        return NO;

    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}



- (void)updateChosenCommitsArray:(NSMutableArray *)array withJumpingJacks:(NSNumber *)jacks{
    if (!jacks) {
        return;
    }
    
    
    BOOL found = NO;
    int index;
    
    for (int i=0; i<array.count; i++) {
        NSDictionary *dict = [array objectAtIndex:i];
        if ([dict objectForKey:@"jacks"]) {
            found = YES;
            index = i;
            break;
        }
    }
    
    if (found) {
        NSMutableDictionary *dict = [[array objectAtIndex:index] mutableCopy];
        NSNumber *current = [dict objectForKey:@"jacks"];
        int total = [current intValue] + [jacks intValue];
        //jacks count
        [dict setObject:[NSNumber numberWithInt:total] forKey:@"jacks"];
        
        //sentence
        [dict setObject:[NSString stringWithFormat:@"Do %i jumping jacks", total] forKey:@"sentence"];
        [array replaceObjectAtIndex:index withObject:dict];
    }
    
    else {
        [array addObject:@{@"sentence": [NSString stringWithFormat:@"Do %i jumping jacks", [jacks intValue]],
                           @"imageNumber": [NSNumber numberWithInt:1],
                           @"jacks": jacks}];
    }
    
    //Save is done in calling method (commitHackChoseOption)
    
}


- (void)commitHackChoseOption:(NSString *)sentence imageOption:(int)imageOpt jacksNumber:(NSNumber *)jacks{
    //NSLog(@"Commit Hack Chosen: %@", sentence);
    
    //Remove used sentence
    NSMutableArray *array = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"unUsedCommitmentDictionary"] mutableCopy];
    if (!array || array.count == 0) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CommitmentHack" ofType:@"plist"];
        array = [NSMutableArray arrayWithContentsOfFile:path];
    }
    
    if (array.count > 0) {
        [array removeObjectAtIndex:0];
    }
    
    //Set Chosen Option
    NSMutableArray *chosen = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"chosenCommitmentArray"] mutableCopy];
    if (!chosen) {
        chosen = [[NSMutableArray alloc] init];
    }
    
    if (sentence != nil) {
        NSDictionary *dic = @{@"sentence": sentence, @"imageNumber": [NSNumber numberWithInt:imageOpt]};
        [chosen addObject:dic];
    }
    
    
    //Increment Jumping Jacks Count
    [self updateChosenCommitsArray:chosen withJumpingJacks:jacks];
    
    
    
    //Save with Date
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"unUsedCommitmentDictionary"];
    [[NSUserDefaults standardUserDefaults] setObject:chosen forKey:@"chosenCommitmentArray"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"commitmentLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setCommitmentDict:(NSDictionary *)dic atIndex:(int)index{
    NSMutableArray *array = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"chosenCommitmentArray"] mutableCopy];
    if (array) {
        [array replaceObjectAtIndex:index withObject:dic];
    }
    
    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"chosenCommitmentArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma Commitment2

- (BOOL)isCommitment2DoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"commitment2LastDoneDate"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}


- (void)commitment2Done{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"commitment2LastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




#pragma Progress

- (BOOL)commitmentsCompletedToday{
    //BOOL ret = NO;
    NSArray *commits = [self chosenCommitmentArray];
    
    for (NSDictionary *dict in commits) {
        //NSDate *today = [NSDate date];
        //NSDate *day = [dict objectForKey:@"date"];
        NSDate *checkDay = [dict objectForKey:@"checkedTodayDate"];
        
        
        if (![checkDay isToday]) {
            return NO;
        }
        
    }
    
    return YES;
}




#pragma Imprinter
- (UIImage *)imprinterBedImage{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImprinterBed.jpg"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    if (fileExists) {
        return [UIImage imageWithContentsOfFile:filePath];
    }
    
    return nil;
}

- (BOOL)haveImprinterBedImg{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImprinterBed.jpg"];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    if (!fileExists) {
        return NO;
    }
    
    return YES;
}

- (BOOL)isImprinterDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"imprinterLastDoneDate"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}



- (void)imprinterHackDoneToday{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"imprinterLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




#pragma Primer

- (NSString *)unUsedPrimerString{
    NSArray *array = [[NSUserDefaults standardUserDefaults] arrayForKey:@"unUsedPrimer"];
    if (!array || array.count == 0) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"PrimerHack" ofType:@"plist"];
        array = [NSArray arrayWithContentsOfFile:path];
        return [array firstObject];
    }
    
    else if ([array count] > 0)
        return [array objectAtIndex:[self primerCount]];
    
    return nil;
}

- (int)primerCount{
    NSNumber *count = [[NSUserDefaults standardUserDefaults] objectForKey:@"primerCount"];
    if (!count)
        return 0;
    
    NSLog(@"count: %i", [count intValue]);
    return [count intValue];
}

- (void)setPrimerCount:(int)count{
    NSNumber *num = [NSNumber numberWithInt:count];
    [[NSUserDefaults standardUserDefaults] setObject:num forKey:@"primerCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (BOOL)isPrimerDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"primerLastDoneDate"];
    if (!saveDate)
        return NO;
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    //NSLog(@"Primer Date: %@", [[cal dateFromComponents:saveDayComp] description]);
    //NSLog(@"Today Date: %@", [[cal dateFromComponents:todayComp] description]);

    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]]) {
//        if ([self primerCount] < 5) {
//            return NO;
//        }
    
        return YES;
    }
    
    return NO;
}




- (void)primerHackDone{
    //Remove used word
    NSMutableArray *array = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"unUsedPrimer"] mutableCopy];
    if (!array || array.count == 0) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"PrimerHack" ofType:@"plist"];
        array = [NSMutableArray arrayWithContentsOfFile:path];
    }
    
//    if (array.count > 0) {
//        [array removeObjectAtIndex:0];
//    }
    
    //Increment Primer Count
    NSNumber *count = [[NSUserDefaults standardUserDefaults] objectForKey:@"primerCount"];
    if (!count) {
        count = [NSNumber numberWithInt:0];
    }
//    else if ([count intValue] == 5)
//        count = [NSNumber numberWithInt:0];

    
    count = [NSNumber numberWithInt:[count intValue]+1];
    
    
    //Save with Date
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"unUsedPrimer"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"primerLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"primerCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




#pragma SleepRitual
- (BOOL)isSleepRitualInstrDone{
    NSNumber *boolNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"sleepRitualInstrDone"];
    return [boolNumber boolValue];
}

- (void)setSleepRitualInstrDone:(BOOL)done{
    NSNumber *boolNumber = [NSNumber numberWithBool:done];
    
    [[NSUserDefaults standardUserDefaults] setObject:boolNumber forKey:@"sleepRitualInstrDone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isSleepRitualDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"sleepRitualLastDoneDate"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}

- (void)sleepRitualDone{
    
    //Save with Date
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"sleepRitualLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}





#pragma Tiredness Test

- (NSNumber *)tirednessDuration{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"tirednessDuration"];
}

- (NSNumber *)tirednessLastDoneDate{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"tirednessLastDoneDate"];
}

- (BOOL)istirednessDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"tirednessLastDoneDate"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}


- (void)tirednessDoneWithSpeed:(NSNumber *)time{
    NSLog(@"tirednessDoneWithSpeed");
    NSMutableArray *array = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"tirednessArray"] mutableCopy];
    if (!array) {
        array = [[NSMutableArray alloc] init];
    }
    
    [array addObject:@{@"duration": time, @"date": [NSDate date]}];
    
    
    NSNumber *count = [[NSUserDefaults standardUserDefaults] objectForKey:@"tirednessCount"];
    if (!count || [count intValue] == 2) {
        count = [NSNumber numberWithInt:0];
    
    } else {
        count = [NSNumber numberWithInt:[count intValue] + 1];
    }

    
    //Save with Date
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"tirednessLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"tirednessArray"];
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"tirednessCount"];

    [[NSUserDefaults standardUserDefaults] synchronize];
}



- (BOOL)istirednessComplete{
    NSNumber *count = [[NSUserDefaults standardUserDefaults] objectForKey:@"tirednessCount"];

    if (!count || [count intValue] < 2) {
        return NO;
    }
    
    return YES;
}

- (void)setTirednessCount:(int)num{
    NSNumber *count = [[NSUserDefaults standardUserDefaults] objectForKey:@"tirednessCount"];
    if (!count)
        count = [NSNumber numberWithInt:0];

    count = [NSNumber numberWithInt:num];
    
    [[NSUserDefaults standardUserDefaults] setObject:count forKey:@"tirednessCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (NSNumber *)tirednessCount{
    NSNumber *count = [[NSUserDefaults standardUserDefaults] objectForKey:@"tirednessCount"];
    if (!count)
        count = [NSNumber numberWithInt:0];
    
    return count;
}





#pragma Relax
- (BOOL)isRelaxDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"relaxLastDoneDate"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}


- (void)relaxHackDoneToday{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"relaxLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma Personality
- (BOOL)isPersonalityInstrDone{
    NSNumber *boolNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"personalityInstrDone"];
    return [boolNumber boolValue];
}

- (void)setPersonalityInstrDone:(BOOL)done{
    NSNumber *boolNumber = [NSNumber numberWithBool:done];
    
    [[NSUserDefaults standardUserDefaults] setObject:boolNumber forKey:@"personalityInstrDone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)personalityQuestions{
    NSArray *arr_Of_Questions=[[NSArray alloc]initWithObjects:@"You are almost never late for your appointments",@"You like to be engaged in an active and fast-paced job",@"You enjoy having a wide circle of acquaintances",@"You feel involved when watching TV shows",@"You are more interested in a general idea than in the details of its realization",@"You tend to be unbiased even if this might endanger your good relations with people",@"Strict observance of the established rules is likely to prevent a good outcome",@"It's difficult to get you excited",@"It is in your nature to assume responsibility",@"You often think about humankind and its destiny",@"You believe the best decision is one that can be easily changed",@"Objective criticism is always useful in any activity",@"You prefer to act immediately rather than speculate about various options",@"You trust reason rather than feelings",@"You are inclined to rely more on improvisation than on prior planning",@"You spend your leisure time actively socializing with a group of people, attending parties, shopping, etc.",@"You feel involved when watching TV shows",@"You usually plan your actions in advance",@"Your actions are frequently influenced by emotions",@"You are a person somewhat reserved and distant in communication",@"You know how to put every minute of your time to good purpose",@"You readily help people while asking nothing in return",@"You often contemplate the complexity of life",@"After prolonged socializing you feel you need to get away and be alone",@"You often do jobs in a hurry",@"You easily see the general principle behind specific occurrences",@"You frequently and easily express your feelings and emotions",@"You find it difficult to speak loudly",@"You get bored if you have to read theoretical books",@"You tend to sympathize with other people",@"You value justice higher than mercy",@"You rapidly get involved in the social life of a new workplace",@"The more people with whom you speak, the better you feel",@"You tend to rely on your experience rather than on theoretical alternatives",@"As a rule, you proceed only when you have a clear and detailed plan",@"You easily empathize with the concerns of other people",@"You often prefer to read a book than go to a party",@"You enjoy being at the center of events in which other people are directly involved",@"You are more inclined to experiment than to follow familiar approaches",@"You avoid being bound by obligations",@"You are strongly touched by stories about people's troubles",@"Deadlines seem to you to be of relative, rather than absolute, importance",@"You prefer to isolate yourself from outside noises",@"It's essential for you to try things with your own hands",@"You think that almost everything can be analyzed",@"For you, no surprises is better than surprises - bad or good ones",@"You take pleasure in putting things in order",@"You feel at ease in a crowd",@"You have good control over your desires and temptations",@"You easily understand new theoretical principles",@"The process of searching for a solution is more important to you than the solution itself",@"You usually place yourself nearer to the side",@"than in the center of a room",@"When solving a problem you would rather follow a familiar approach than seek a new one",@"You try to stand firmly by your principles",@"A thirst for adventure is close to your heart",@"You prefer meeting in small groups over interaction with lots of people",@"When considering a situation you pay more attention to the current situation and less to a possible sequence of events",@"When solving a problem you consider the rational approach to be the best",@"You find it difficult to talk about your feelings",@"You often spend time thinking of how things could be improved",@"Your decisions are based more on the feelings of a moment than on the thorough planning",@"You prefer to spend your leisure time alone or relaxing in a tranquil atmosphere",@"You feel more comfortable sticking to conventional ways",@"You are easily affected by strong emotions",@"You are always looking for opportunities",@"Your desk, workbench, etc. is usually neat and orderly",@"As a rule, current preoccupations worry you more than your future plans",@"You get pleasure from solitary walks",@"It is easy for you to communicate in social situations",@"You are consistent in your habits",@"You willingly involve yourself in matters which engage your sympathies",@"You easily perceive various ways in which events could develop", nil];
    
    return arr_Of_Questions;
}


- (BOOL)isPersonalityDoneToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"personalityLastDoneDate"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}


- (void)personalityHackDoneToday{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"personalityLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




#pragma Sleep Data
//helper
- (NSMutableDictionary *)sleepRecordDictionary{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sleepPath = [rootPath stringByAppendingPathComponent:@"SleepRecord.plist"];
    NSMutableDictionary *sleepDict;
    NSMutableArray *sleepArray;
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:sleepPath];
    if (!fileExists) {
        sleepDict = [[NSMutableDictionary alloc] init];
        sleepArray = [[NSMutableArray alloc] init];
        [sleepDict setObject:sleepArray forKey:@"record"];
        
    } else{
        sleepDict = [[NSMutableDictionary alloc] initWithContentsOfFile:sleepPath];
        sleepArray = [sleepDict objectForKey:@"record"];
    }

    return sleepDict;
}

//helper
- (NSString *)sleepKeyForSleepAction:(SleepAction)sleepType{
    NSString *result = nil;
    
    switch (sleepType) {
        case SleepAction_InBed:
            result = @"inBed";
            break;
            
        case SleepAction_OutBed:
            result = @"outBed";
            break;
            
        case SleepAction_EyesClosed:
            result = @"eyesClosed";
            break;
            
        case SleepAction_EyesOpened:
            result = @"eyesOpened";
            break;
            
        default:
            [NSException raise:NSGenericException format:@"Unexpected FormatType."];
            break;
    }
    
    return result;
}

//helper
- (void)averageDailySleep{
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];
    NSMutableArray *sleepArray = [sleepDict objectForKey:@"record"];
    
    
    NSDate *eClosed = nil, *eOpened = nil;
    NSTimeInterval duration, total = 0, avg = 0;
    NSMutableArray *durArray = [[NSMutableArray alloc] init];
    
    //Create Array of All secs asleep
    for (NSDictionary *dict in sleepArray) {
        if ([dict objectForKey:@"eyesClosed"]) {
            eClosed = [dict objectForKey:@"eyesClosed"];
            eOpened = nil;
        }
        
        else if ([dict objectForKey:@"eyesOpened"]){
            if (eClosed != nil) {
                eOpened = [dict objectForKey:@"eyesOpened"];

                duration = [eOpened timeIntervalSinceDate:eClosed];
                [durArray addObject:[NSNumber numberWithDouble:duration]];
                
                
                //NSLog(@"Closed: %@; Opened: %@", eClosed, eOpened);
                //NSLog(@"Duration: %f", duration);
                
            }
            
            //nil out times
            eClosed = nil, eOpened = nil;
        }
    }
    
    
    //Get Average of all secs
    for (NSNumber *durNumber in durArray) {
        total+= [durNumber doubleValue];
    }
    
    avg = total/[durArray count];
    
    //NSLog(@"Average: %f", avg);
    
    
    //Save
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sleepPath = [rootPath stringByAppendingPathComponent:@"SleepRecord.plist"];
    
    [sleepDict setObject:[NSNumber numberWithDouble:avg] forKey:@"averageDailySleepSecs"];
    [sleepDict writeToFile:sleepPath atomically:YES];

}

//helper
- (void)averageDailyBed{
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];
    NSMutableArray *sleepArray = [sleepDict objectForKey:@"record"];
    
    
    NSDate *inBed = nil, *outBed = nil;
    NSTimeInterval duration, total = 0, avg = 0;
    NSMutableArray *durArray = [[NSMutableArray alloc] init];
    
    //Create Array of All secs asleep
    for (NSDictionary *dict in sleepArray) {
        if ([dict objectForKey:@"inBed"]) {
            inBed = [dict objectForKey:@"inBed"];
            outBed = nil;
        }
        
        else if ([dict objectForKey:@"outBed"]){
            if (inBed != nil) {
                outBed = [dict objectForKey:@"outBed"];
                
                duration = [outBed timeIntervalSinceDate:inBed];
                [durArray addObject:[NSNumber numberWithDouble:duration]];
                
                
                //NSLog(@"In Bed: %@; Out Bed: %@", inBed, outBed);
                //NSLog(@"Duration: %f", duration);
                
            }
            
            //nil out times
            inBed = nil, outBed = nil;
        }
    }
    
    
    //Get Average of all secs
    for (NSNumber *durNumber in durArray) {
        total+= [durNumber doubleValue];
    }
    
    avg = total/[durArray count];
    
    //NSLog(@"Average: %f", avg);
    
    
    //Save
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sleepPath = [rootPath stringByAppendingPathComponent:@"SleepRecord.plist"];
    
    [sleepDict setObject:[NSNumber numberWithDouble:avg] forKey:@"averageDailyBedSecs"];
    [sleepDict writeToFile:sleepPath atomically:YES];
    
}


- (NSNumber *)averageDailySleepSecs{
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];

    return [sleepDict objectForKey:@"averageDailySleepSecs"];
}

- (NSNumber *)averageDailyBedSecs{
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];
    
    return [sleepDict objectForKey:@"averageDailyBedSecs"];
}


- (BOOL)isManualSleepDataAskedToday{
    NSDate *saveDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"manualSleepDataAskedToday"];
    if (!saveDate)
        return NO;
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *saveDayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:saveDate];
    
    if ([[cal dateFromComponents:todayComp] isEqualToDate:[cal dateFromComponents:saveDayComp]])
        return YES;
    
    return NO;
}

- (BOOL)isForcingManualSleep{
    NSNumber *forceNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"forceManualSleep"];
    if (!forceNumber)
        return NO;
    
    return [forceNumber boolValue];
}



- (void)sleepRecordBegin{
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];
    if (![sleepDict objectForKey:@"beginDate"]){
        [sleepDict setObject:[NSDate date] forKey:@"beginDate"];

        //Save
        NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *sleepPath = [rootPath stringByAppendingPathComponent:@"SleepRecord.plist"];
        [sleepDict writeToFile:sleepPath atomically:YES];
    }
}

- (void)sleepRecordAverageCalculate{
    //return;
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];
    NSDate *begdt = [sleepDict objectForKey:@"beginDate"];
    NSDate *weekdt = [begdt dateByAddingDays:7];
    
    //Check Today is >= Begdt + 7
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    NSDateComponents *weekDateComp = [cal components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:weekdt];
    
    
    
    
    if ([[cal dateFromComponents:todayComp] isLaterThanDate:[cal dateFromComponents:weekDateComp]]) {
        //calculate average sleep times
        [self averageDailySleep];
        [self averageDailyBed];
    }

        
        
    //NSLog(@"BegDt: %@; 7 Days After: %@", begdt, weekdt);
    
}

- (void)recordSleepAction:(SleepAction)sleepType{
    //Get Sleep Record
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];
    NSMutableArray *sleepArray = [sleepDict objectForKey:@"record"];
    
    //Add New Entry (Dict)
    NSString *key = [self sleepKeyForSleepAction:sleepType];
    NSMutableDictionary *entry = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSDate date], key, nil];
    [sleepArray addObject:entry];
    
    //Save
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sleepPath = [rootPath stringByAppendingPathComponent:@"SleepRecord.plist"];
    
    [sleepDict setObject:sleepArray forKey:@"record"];
    [sleepDict writeToFile:sleepPath atomically:YES];
}

- (void)recordDate:(NSDate *)date forSleepAction:(SleepAction)sleepType{
    //Get Sleep Record
    NSMutableDictionary *sleepDict = [self sleepRecordDictionary];
    NSMutableArray *sleepArray = [sleepDict objectForKey:@"record"];
    
    //Add New Entry (Dict)
    NSString *key = [self sleepKeyForSleepAction:sleepType];
    NSMutableDictionary *entry = [[NSMutableDictionary alloc] initWithObjectsAndKeys:date, key, nil];
    [sleepArray addObject:entry];
    
    //Save
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sleepPath = [rootPath stringByAppendingPathComponent:@"SleepRecord.plist"];
    
    [sleepDict setObject:sleepArray forKey:@"record"];
    [sleepDict writeToFile:sleepPath atomically:YES];
}

- (void)manualSleepDataAskedToday{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"manualSleepDataAskedToday"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)forceManualSleep{
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"manualSleepDataAskedToday"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"forceManualSleep"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)removeForceManualSleep{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"forceManualSleep"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}





#pragma mark - Private Methods

- (void)setUp
{
    [self loadUnAnsweredHackSentencesIfNecessary];
    [self loadAnseredHackSentenesIfNecessary];
    
    //[self loadUnAskedHighlighterHackSentencesIfNecessary];
}

- (void)loadUnAnsweredHackSentencesIfNecessary
{
    if (!self.unAnsweredHackSentences) {
        self.unAnsweredHackSentences = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"UnAnsweredHackSentences"] mutableCopy];
        if (!self.unAnsweredHackSentences) {
            NSString *path = [[NSBundle mainBundle] pathForResource:@"hacksentences" ofType:@"plist"];
            self.unAnsweredHackSentences = [[NSArray arrayWithContentsOfFile:path] mutableCopy];
        }
    }
}

- (void)loadAnseredHackSentenesIfNecessary
{
    if (!self.answeredHackSentences) {
        self.answeredHackSentences = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"AnsweredHackSentences"] mutableCopy];
        if (!self.answeredHackSentences) {
            self.answeredHackSentences = [NSMutableDictionary dictionary];
            NSLog(@"%@",self.answeredHackSentences);
        }
    }
}


/*
#pragma mark - Private Methods Hacks

- (void)loadUnAskedHighlighterHackSentencesIfNecessary{
    if (!self.unAskedHighlightedHackSentences) {
        self.unAskedHighlightedHackSentences = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"unAskedHighlightedHackSentences"] mutableCopy];
        if (!self.unAskedHighlightedHackSentences) {
            //NSLog(@"Load from Plist");
            NSString *path = [[NSBundle mainBundle] pathForResource:@"HighlighterHack" ofType:@"plist"];
            self.unAskedHighlightedHackSentences = [[NSArray arrayWithContentsOfFile:path] mutableCopy];
        }
    }
}
*/




#pragma Other

- (UIImage *)randomScoreImage{
    NSInteger min = 1;
    NSInteger max = 8;
    NSInteger random = min + arc4random_uniform(max - min + 1);
    UIImage *image;
    
    switch (random) {
        case 1:
            image = [UIImage imageNamed:@"Score_Awesome.png"];
            break;
            
        case 2:
            image = [UIImage imageNamed:@"Score_Bravo.png"];
            break;
            
        case 3:
            image = [UIImage imageNamed:@"Score_GreatJob.png"];
            break;
            
        case 4:
            image = [UIImage imageNamed:@"Score_Nice.png"];
            break;
            
        case 5:
            image = [UIImage imageNamed:@"Score_Splendid.png"];
            break;
            
        case 6:
            image = [UIImage imageNamed:@"Score_Superb.png"];
            break;
            
        case 7:
            image = [UIImage imageNamed:@"Score_Sweet.png"];
            break;
        
        case 8:
            image = [UIImage imageNamed:@"Score_Woot.png"];
            break;
            
        default:
            image = [UIImage imageNamed:@"Score_Sweet.png"];
            break;
    }
    
    
    return image;
}



#pragma Day ONE Reset
- (void)dayOneReset{
    
    //Remove Values from NSUserDefaults
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"firstLaunch"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"launched"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isDoneFirstPL"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PersonalityHackNumber"];


    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"highlighterLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"highlighter2LastDoneDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"askedHighlightedHackSentences"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"highlighterScore"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unAskedHighlightedHackSentences"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"chosenCommitmentArray"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unUsedCommitmentDictionary"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"commitmentLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"commitment2LastDoneDate"];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"imprinterLastDoneDate"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"relaxLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"personalityLastDoneDate"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"manualSleepDataAskedToday"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"prescribedInBedDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"prescribedOutBedDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"prescribedSleepTimerDate"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unUsedPrimer"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"primerLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"primerCount"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"sleepRitualLastDoneDate"];
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"tirednessLastDoneDate"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"tirednessArray"];



    
    //Delete SleepRecord.plist
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sleepPath = [documentsPath stringByAppendingPathComponent:@"SleepRecord.plist"];
    
    NSError *error;
    BOOL success;
    
    success = [fileManager removeItemAtPath:sleepPath error:&error];
    
    //Delete CommitSignature.jpg
    NSString *commitSig = [documentsPath stringByAppendingPathComponent:@"CommitSignature.jpg"];
    success = [fileManager removeItemAtPath:commitSig error:&error];
    
    //Delete ImprinterBed.jpg
    NSString *imprinterPic = [documentsPath stringByAppendingPathComponent:@"ImprinterBed.jpg"];
    success = [fileManager removeItemAtPath:imprinterPic error:&error];
    
    
    
    
    //Delete all LocalNotifications
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    
    //Re - Add
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"firstLaunch"];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launched"];

    
    NSMutableDictionary *currWeekDict = [[NSMutableDictionary alloc] init];
    [currWeekDict setObject:[NSDate date] forKey:@"day"];
    [currWeekDict setObject:[NSNumber numberWithInt:0] forKey:@"dayCount"];
    [currWeekDict setObject:[NSNumber numberWithBool:NO] forKey:@"dayComplete"];
    [[NSUserDefaults standardUserDefaults] setObject:currWeekDict forKey:@"currentWeek"];
    
    
    
    //save all
    [[NSUserDefaults standardUserDefaults] synchronize];
}





@end
