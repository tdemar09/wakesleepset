//
//  CommitHackViewController.m
//  Myndus
//
//  Created by Troy DeMar on 10/1/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "CommitHackViewController.h"

@interface CommitHackViewController ()

@end

@implementation CommitHackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.instrImageView.image = [UIImage imageNamed:@"Hack_Mirror_Intro_Day.png"];

}



#pragma mark - IBAction Methods



@end
