//
//  SendMail.h
//  Mynd
//
//  Created by Shuaib on 24/06/2014.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKPSMTPMessage.h"
#import "NSData+Base64Additions.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#include <netinet/in.h>

@interface SendMail : NSObject <SKPSMTPMessageDelegate> {
    SKPSMTPMessage *aMsg;
}

- (id)initWithMessage:(NSString *)body withDelegate:(id<SKPSMTPMessageDelegate>)delegate withTag:(NSInteger)tag;

- (void)send;


    
- (void)sendMessageInBack:(NSString *)body withDelegate:(id)delegate withTag:(NSInteger)tag;
- (void)sendMessageInBack:(NSString *)body;

//- (void)messageSent:(SKPSMTPMessage *)message;

@end
