//
//  UserDefaultsStore.h
//  Mynd
//
//  Created by Shuaib on 01/07/2014.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultsStore : NSObject

+ (UserDefaultsStore *)shared;

- (NSString *)unAnsweredHackSentence;
- (void)saveAnswer:(BOOL)answer forHackSenctence:(NSString *)sentence;


- (NSDate *)firstLaunch;
- (NSString *)username;
- (void)setUsername:(NSString *)username;


#pragma Current Week
- (void)setToday;
- (BOOL)isToday;

- (NSNumber *)currentWeekDayCount;
- (BOOL)isCurrentWeekDayComplete;
- (void)setCurrentWeekDayComplete:(BOOL)complete;

- (NSNumber *)currentWeekHackSequenceCountForExperiment:(Experiment)exp;
- (HackSequence)currentWeekHackSequenceForExperiment:(Experiment)exp;
- (void)setCurrentWeekHackSequence:(HackSequence)sequence forExperiment:(Experiment)exp count:(int)count;



#pragma Emails
- (void)addEmail:(NSString *)email;
- (void)sendEmails;


#pragma Mirror
- (BOOL)isMirrorInstrDone;
- (void)setMirrorInstrDone:(BOOL)done;


#pragma PrescribedSleepTimer (TurboSleep)
- (BOOL)isPrescribedSleepTimerDone;
- (BOOL)shouldDisplayPrescribedSleep;
- (NSDate *)prescribedInBedDate;
- (NSDate *)prescribedOutBedDate;

- (void)setPrescribedSleepTime:(NSDate *)inBedDate wakeDate:(NSDate *)outBedDate;
- (void)prescribedSleepTimerDone;
- (void)forceDisplayPrescribedSleep;


#pragma Highlighter
- (BOOL)isHighlighterInstrDone;
- (void)setHighlighterInstrDone:(BOOL)done;
- (NSString *)unAskedHighlighterSentence;
- (NSInteger)generateHighlighterScore;
- (NSInteger)highlighterTotal;
- (BOOL)isHighlighterDoneToday;

- (void)highlighterHackChoseWords:(NSArray *)wordsArray;
- (void)incrementHighlighterScore:(NSInteger)score;


#pragma Highlighter2(Hack done twice per day)
- (BOOL)isHighlighter2DoneToday;
- (void)highlighter2Done;


#pragma Commitment
- (BOOL)isCommitmentInstrDone;
- (void)setCommitmentInstrDone:(BOOL)done;
- (BOOL)hasUnUsedCommitmentDictionary;
- (NSDictionary *)unUsedCommitmentDictionary;
- (NSArray *)chosenCommitmentArray;
- (NSArray *)commitmentPercentagesArray;
- (BOOL)isCommitmentPlusOnTop;
- (UIImage *)commitmentSigImage;
- (BOOL)haveCommitmentSignature;
- (BOOL)isCommitmentDoneToday;

//- (void)commitHackChoseOption:(NSString *)sentence;
- (void)commitHackChoseOption:(NSString *)sentence imageOption:(int)imageOpt jacksNumber:(NSNumber *)jacks;
- (void)setCommitmentDict:(NSDictionary *)dic atIndex:(int)index;



#pragma Commitment2(Hack done twice per day)
- (BOOL)isCommitment2DoneToday;
- (void)commitment2Done;



#pragma Progress
- (BOOL)commitmentsCompletedToday;



#pragma Imprinter
- (UIImage *)imprinterBedImage;
- (BOOL)haveImprinterBedImg;
- (BOOL)isImprinterDoneToday;

- (void)imprinterHackDoneToday;



#pragma Primer
- (NSString *)unUsedPrimerString;
- (int)primerCount;
- (void)setPrimerCount:(int)count;
- (BOOL)isPrimerDoneToday;
- (void)primerHackDone;




#pragma SleepRitual
- (BOOL)isSleepRitualInstrDone;
- (void)setSleepRitualInstrDone:(BOOL)done;
- (BOOL)isSleepRitualDoneToday;
- (void)sleepRitualDone;




#pragma Tiredness Test
- (NSNumber *)tirednessDuration;
- (NSNumber *)tirednessLastDoneDate;
- (BOOL)istirednessDoneToday;
- (void)tirednessDoneWithSpeed:(NSNumber *)time;
- (BOOL)istirednessComplete;
- (void)setTirednessCount:(int)num;
- (NSNumber *)tirednessCount;



#pragma Relax
- (BOOL)isRelaxDoneToday;

- (void)relaxHackDoneToday;



#pragma Personality
- (BOOL)isPersonalityInstrDone;
- (void)setPersonalityInstrDone:(BOOL)done;
- (NSArray *)personalityQuestions;
- (BOOL)isPersonalityDoneToday;
- (void)personalityHackDoneToday;



#pragma Sleep Data
- (NSNumber *)averageDailySleepSecs;
- (NSNumber *)averageDailyBedSecs;
- (BOOL)isManualSleepDataAskedToday;
- (BOOL)isForcingManualSleep;

- (void)sleepRecordBegin;
- (void)sleepRecordAverageCalculate;
- (void)recordSleepAction:(SleepAction)sleepType;
- (void)recordDate:(NSDate *)date forSleepAction:(SleepAction)sleepType;
- (void)manualSleepDataAskedToday;
- (void)forceManualSleep;
- (void)removeForceManualSleep;



#pragma Other
- (UIImage *)randomScoreImage;



#pragma Day ONE Reset
- (void)dayOneReset;

@end
