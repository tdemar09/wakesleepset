//
//  CommitSelectViewController.h
//  Mynd
//
//  Created by Troy DeMar on 7/22/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HackBaseViewController.h"

@interface CommitSelectViewController : HackBaseViewController {
    IBOutlet UIButton *naButton;

}

@property (nonatomic, strong) UIButton *naButton;
@property (nonatomic) NSInteger viewOption;


@end
