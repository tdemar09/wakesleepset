//
//  CommitDisplayViewController.h
//  Mynd
//
//  Created by Troy DeMar on 7/23/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HackBaseViewController.h"

@interface CommitDisplayViewController : HackBaseViewController {
    IBOutlet UIImageView *answerImgView;
}

@property (nonatomic, strong) UIImageView *answerImgView;
@property (nonatomic) NSInteger viewOption, answerOption;

@end
