//
//  HackPersonalityViewController.h
//  Mynd
//
//  Created by Sanjay on 29/07/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HackBaseViewController.h"

//#import "HackPersonality2ViewController.h"

@interface HackPersonalityViewController : HackBaseViewController
{
    IBOutlet UIButton *btn_Continue;

    IBOutlet UIButton *btn_1_Yes;
    IBOutlet UIButton *btn_2_Yes;
    IBOutlet UIButton *btn_3_Yes;
    IBOutlet UIButton *btn_4_Yes;
    IBOutlet UIButton *btn_5_Yes;
    IBOutlet UIButton *btn_1_No;
    IBOutlet UIButton *btn_2_No;
    IBOutlet UIButton *btn_3_No;
    IBOutlet UIButton *btn_4_No;
    IBOutlet UIButton *btn_5_No;
    IBOutlet UILabel *lbl_PersonQ1;
    IBOutlet UILabel *lbl_PersonQ2;
    IBOutlet UILabel *lbl_PersonQ3;
    IBOutlet UILabel *lbl_PersonQ4;
    IBOutlet UILabel *lbl_PersonQ5;
    NSMutableArray *arrSecondFive;
    IBOutlet UIButton *btnEye;
}
- (IBAction)ClickToRoot:(id)sender;
@end
