//
//  CommitTableViewCell.h
//  Myndus
//
//  Created by Troy DeMar on 10/7/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommitCellDelegate;

@interface CommitTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, strong) IBOutlet UIImageView *imgView, *checkImgView;
@property (nonatomic, strong) IBOutlet UIButton *cellBtn, *checkBtn;

@property (assign, nonatomic) int index;
@property (nonatomic,weak) id <CommitCellDelegate> delegate;


- (void)displayCurrent;
- (BOOL)isCommitmentCheckedToday;

@end




@protocol CommitCellDelegate <NSObject>
@optional
- (void)commitCell:(CommitTableViewCell *)cell didTapCellButton:(UIButton *)button atIndex:(int)index;
- (void)commitCell:(CommitTableViewCell *)cell didTapCheckButton:(UIButton *)button atIndex:(int)index;

@end

