//
//  MirrorHackViewController.m
//  Myndus
//
//  Created by Troy DeMar on 10/1/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "MirrorHackViewController.h"
#import "UserDefaultsStore.h"
#import "Reachability.h"

@interface MirrorHackViewController ()
@property (nonatomic) int tiredCount;
@property (nonatomic) float ans1, ans2;
@property (nonatomic) BOOL started;
@end

@implementation MirrorHackViewController
@synthesize imgView, cameraView, sliderBtn, contineBtn, exitBtn, label, leftLbl, rightLbl, zeroLbl;
@synthesize waitView, sliderView, sliderBarView, zeroLineView, slider;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidLoad{
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    // Do any additional setup after loading the view.
    //[self updateBaseLayouts];
    
    
    //Only show if before 8:30AM
    if (![self isBefore830Am])
        [self endTimers];

    
    self.instrView.hidden = YES;
    
    if (self.sequence == HackSequenceNew) {
        self.view.backgroundColor = [UIColor whiteColor];
        self.label.textColor = [UIColor blackColor];
        
    }
    else if (self.sequence == HackSequenceSleep) {
        self.view.backgroundColor = [UIColor blackColor];
        self.label.textColor = [UIColor whiteColor];
    }

    
    fps = 30.0f;
    
    
    if (![[UserDefaultsStore shared] isMirrorInstrDone]) {
        [[UserDefaultsStore shared] setMirrorInstrDone:YES];
        self.started = YES;

        timer_instr = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(showInstructions) userInfo:nil repeats:NO];
    
    }else {
        self.instrView.hidden = YES;
        
        if (!self.started)
            [self start];
    }
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //NSLog(@"viewWillAppear");
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //NSLog(@"viewDidAppear");
    
    [self initializeCamera];

    [self.navView addSubview:navSlider];
    
    
    if (!self.instrView.hidden) {
        timer_instr = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(showInstructions) userInfo:nil repeats:NO];
        
    }else {
        if (!self.started) {
            NSLog(@"restart");
            [self reset];
            [self start];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [self reset];
    
}



- (BOOL)isBefore830Am{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    NSInteger currentHour = [components hour];
    NSInteger currentMinute = [components minute];
    //NSInteger currentSecond = [components second];
    
    //NSLog(@"Datetime: %@", [NSDate date].description);
    //NSLog(@"Hour: %i; Min: %i; Sec: %i", currentHour, currentMinute, currentSecond);
    
    if (currentHour < 8 || (currentHour == 8 && currentMinute < 30) ) {
        return YES;
    }
    
    return NO;
}

- (void)initializeCamera {
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
	session.sessionPreset = AVCaptureSessionPresetPhoto;
	
	AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
	captureVideoPreviewLayer.frame = self.cameraView.bounds;
	[self.cameraView.layer addSublayer:captureVideoPreviewLayer];
	
    UIView *view = [self cameraView];
    CALayer *viewLayer = [view layer];
    [viewLayer setMasksToBounds:YES];
    
    CGRect bounds = [view bounds];
    [captureVideoPreviewLayer setFrame:bounds];
    
    
    //AVCaptureConnection *previewLayerConnection = captureVideoPreviewLayer.connection;
    
    
    
    //AVCaptureDevice *backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionFront)
            camera = device;
    }
    
//    [camera lockForConfiguration:nil];
//    [camera setActiveVideoMinFrameDuration:CMTimeMake(1, fps)];
//    [camera setActiveVideoMaxFrameDuration:CMTimeMake(1, fps)];
//    [camera unlockForConfiguration];
    
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:camera error:&error];
    if (input) {
        [session addInput:input];
    }
    
    
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
    
    [session addOutput:stillImageOutput];
    
	[session startRunning];
    
}




- (void)showInstructions{
    NSLog(@"showInstructions");
    [timer_instr invalidate];
    
    if (self.sequence == 2)
        self.instrImageView.image = [UIImage imageNamed:@"Hack_Mirror_Intro_Day.png"];
    
    else if (self.sequence == 0)
        self.instrImageView.image = [UIImage imageNamed:@"Hack_Mirror_Intro_Night.png"];
    
    
    self.instrView.hidden = NO;

    
    
    //self.instrImageView.image = [UIImage imageNamed:@"Hack_Mirror_Intro_Day.png"];
    //self.instrView.hidden = NO;
    //contineBtn.hidden = NO;

    //imgView.hidden = NO;
    

}


- (void)start{
    NSLog(@"start");
    self.started = YES;
    imgView.hidden = YES;
    
    
    timer_min = [NSTimer scheduledTimerWithTimeInterval:70 target:self selector:@selector(endTimers) userInfo:nil repeats:NO];
    secsTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(textChange) userInfo:nil repeats:YES];
    
    if (self.sequence == HackSequenceSleep) {
        timer_dec = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(slowFPS) userInfo:nil repeats:YES];
    }
    
}

- (void)slowFPS{

    
    if (fps > 24) {
        fps = fps - 3;
        
    }
    
    else if (fps > 10) {
        fps = fps - 2;
        
    }
    else if (fps-2>0) {
        fps = fps - 0.5f;
        
        
        
        [camera lockForConfiguration:nil];
        [camera setActiveVideoMinFrameDuration:CMTimeMake(1, fps)];
        [camera setActiveVideoMaxFrameDuration:CMTimeMake(1, fps)];
        [camera unlockForConfiguration];
    }
    
    NSLog(@"New Slow FPS: %f", fps);
    
}

- (void)endTimers{
    [timer_min invalidate];
    timer_min = nil;
    
    [timer_dec invalidate];
    timer_dec = nil;
    
    [secsTimer invalidate];
    secsTimer = nil;
    
    self.started = NO;
    
    [self close];
}

- (void)textChange{
    secs++;
    NSLog(@"secs: %i", secs);
    
    //HackSequence seq = [[UserDefaultsStore shared] currentWeekHackSequenceForExperiment:Experiment2];
    HackSequence seq = self.sequence;
    
    if (secs == 3) {
        if (seq == HackSequenceSleep)
            [self.label setText:@"Very slowly, sway your head left and right"];
        else
            [self.label setText:@"Make funny faces"];
        
        self.label.hidden = NO;
    }
    
    
    
    else if (secs == 30) {
        if (seq == HackSequenceSleep)
            [self.label setText:@"Slowly close and open your eyes"];
        else
            [self.label setText:@"Smile"];
        
        self.label.hidden = NO;
    }
    else if (secs == 34)
        self.label.hidden = YES;
    
    
    
    else if (secs == 40) {
        if (seq == HackSequenceSleep)
            [self.label setText:@"Yawwnn"];
        else
            [self.label setText:@"Do a hearty laugh"];
        
        self.label.hidden = NO;
    }
    else if (secs == 44)
        self.label.hidden = YES;
    
    
    
    else if (secs == 55) {
        if (seq == HackSequenceSleep)
            [self.label setText:@"Slowly close and open your eyes"];
        else
            [self.label setText:@"Smile"];
        
        self.label.hidden = NO;
    }
    else if (secs == 59) {
        self.label.hidden = YES;
    }
}

- (void)reset{
    secs = 0;
    fps = 30.0f;
    
    self.started = NO;
    self.label.hidden = YES;
    
    [timer_min invalidate];
    timer_min = nil;
    
    [timer_dec invalidate];
    timer_dec = nil;
    
    [secsTimer invalidate];
    secsTimer = nil;
}


//Not Hooked
- (void)email{
    //Hack Sequence
    NSString *sequence;
    HackSequence seq = [[UserDefaultsStore shared] currentWeekHackSequenceForExperiment:Experiment2];
    
    if (seq == HackSequenceSleep)
        sequence = @"Sleep Sequence";
    else
        sequence = @"Non-Sleep Sequence";
    
    //Email String
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
    
    NSString *emailString = [NSString stringWithFormat:@"User: %@ \nExperiment 2 \nHack Sequence: %@ \nCompleted at: %@ \nTiredness Answer 1: %f (0-100) \nTiredness Answer 2: %f (-50-50)",
                             [[UserDefaultsStore shared] username],
                             sequence,
                             [dateFormatter stringFromDate:[NSDate date]],
                             self.ans1,
                             self.ans2];
    
    
    
    //Check on Internet for Email
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        [[UserDefaultsStore shared] addEmail:emailString];
        
        
    } else {
        //NSLog(@"There IS internet connection");
        SendMail *sendMail = [SendMail new];
        
        [sendMail sendMessageInBack:emailString withDelegate:self withTag:0];
        self.waitView.hidden = NO;
    }
    
    
}

- (void)showWait{
    self.waitView.hidden = NO;
}

- (void)close{
    //Enable Phone idle timer
    //[UIApplication sharedApplication].idleTimerDisabled = NO;
    
    [self continueAction:nil];
}







#pragma mark - IBAction Methods
//testing only *************
- (IBAction)forceEnd:(id)sender{
    [self endTimers];
}
// *************************


- (IBAction)instrPlayAction:(id)sender{
    [super instrPlayAction:sender];
    
    //[self close];
    [self start];
}

- (IBAction)showInstrAction:(id)sender{
    [super showInstrAction:sender];
    
    [self reset];
    
    
    [self showInstructions];
}


//Old
- (IBAction)sliderClicked:(id)sender forEvent:(UIEvent*)event{
    UIView *button = (UIView *)sender;
    UITouch *touch = [[event touchesForView:button] anyObject];
    CGPoint location = [touch locationInView:button];
    //NSLog(@"Location in button: %f, %f", location.x, location.y);
    
    CGFloat scale = location.x/button.frame.size.width;
    //NSLog(@"Scale: %f; x100: %f", scale, scale*100);
    
    sliderBarView.hidden = YES;
    sliderBtn.hidden = YES;
    
    if (self.tiredCount == 0)
        slider.value = scale*100;
    else {
        slider.value = scale*100 - 50;
    }
    slider.hidden = NO;
    contineBtn.hidden = NO;
    
}

//Old
- (IBAction)tirednessContinue:(id)sender{
    timer_instr = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(start) userInfo:nil repeats:NO];
}


- (IBAction)continueAction:(id)sender{
    
    if (self.sequence == HackSequenceNew){
        if ([[UserDefaultsStore shared] haveCommitmentSignature])
            [self performSegueWithIdentifier:@"SeqNew_CommitStartSegue" sender:nil];
        
        else
            [self performSegueWithIdentifier:@"SeqNew_CommitSigSegue" sender:nil];
    }
    
    else if (self.sequence == HackSequenceSleep){
        [self performSegueWithIdentifier:@"SeqSleep_SleepSegue" sender:nil];

    }
    
    //SeqSleep_MirrorSegue
    //SeqSleep_SleepSegue
}


#pragma mark - SKPSMTPMessageDelegate

- (void)messageSent:(SKPSMTPMessage *)message{
    NSLog(@"MESSEAGE SENT");
    [self close];
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error{
    
    NSDictionary *parts = [message.parts objectAtIndex:0];
    [[UserDefaultsStore shared] addEmail:[parts objectForKey:kSKPSMTPPartMessageKey]];
    
    
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to send email, will try again on next app open"
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
	NSLog(@"delegate - error(%ld): %@", (long)[error code], [error localizedDescription]);
    
    
    [self close];
}







@end
