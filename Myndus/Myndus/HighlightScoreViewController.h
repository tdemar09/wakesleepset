//
//  HighlightScoreViewController.h
//  Mynd
//
//  Created by Troy DeMar on 7/22/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "HackBaseViewController.h"

@interface HighlightScoreViewController : HackBaseViewController {
    IBOutlet UIImageView *imageView;
    IBOutlet UILabel *todayLbl, *totalLbl;
    NSArray *chosenWordsArray;
}

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *todayLbl, *totalLbl;
@property (nonatomic, strong) NSArray *chosenWordsArray;

@end
