//
//  Constants.h
//  Advice
//
//  Created by Troy DeMar on 12/6/13.
//  Copyright (c) 2013 Advice App LLC. All rights reserved.
//

#ifndef Advice_Constants_h
#define Advice_Constants_h

//#define Segue @"b"

#define kCommitStaticAnswer @"I will not do any of these things.\nI will do nothing."

#define kPersonalityCountKey @"PersonalityHackNumber"


typedef NS_ENUM(NSInteger, SleepAction) {
    SleepAction_InBed           = 0,
    SleepAction_OutBed          = 1,
    SleepAction_EyesClosed      = 2,
    SleepAction_EyesOpened      = 3
};


typedef NS_ENUM(NSInteger, Experiment) {
    Experiment1  = 1,
    Experiment2  = 2
};

typedef NS_ENUM(NSInteger, HackSequence) {
    HackSequenceSleep  = 0,
    HackSequenceAwake  = 1,
    HackSequenceNew    = 2,
    HackSequenceInterventionWake = 3,
    HackSequenceInterventionSleep = 4,
    HackSequenceCommitList      = 5
};




#pragma mark


#endif
