//
//  NameViewController.h
//  Mynd
//
//  Created by Troy DeMar on 9/5/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameViewController : UIViewController <UIActionSheetDelegate, UITextFieldDelegate> {
    IBOutlet UIView *nameView, *optionView;
    IBOutlet UITextField *nameTextField;
    IBOutlet UISlider *slider;
}
@property (strong, nonatomic) UIView *nameView, *optionView;
@property (strong, nonatomic) UITextField *nameTextField;

@end
