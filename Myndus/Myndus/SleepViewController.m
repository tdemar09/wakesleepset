//
//  SleepViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/20/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "SleepViewController.h"
#import "EyesClosedViewController.h"
#import "UserDefaultsStore.h"
//#import "Indicative.h"
@interface SleepViewController ()
@property (nonatomic, strong) IBOutlet AVAudioPlayer *player;
@end

@implementation SleepViewController
@synthesize pauseButton;
@synthesize soundbtn1, soundbtn2, soundbtn3;
@synthesize imgView, pauseImgView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"prepareForSegue");
    
//    if ([segue.identifier isEqualToString:@"highlightScoreSegue"]) {
//    }
    
}



- (void)viewDidLoad{
    [super viewDidLoad];
    //Event *event = [Event createEvent:@"Sleep View" withUniqueId:[appDelegate getUUID]];
    //[Indicative recordEvent:event];
	// Do any additional setup after loading the view.
    
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *setCategoryError = nil;
    
    BOOL success = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    
    if (!success) { /* handle the error condition */ }
    
    
    NSError *activationError = nil;
    success = [audioSession setActive:YES error:&activationError];
    
    if (!success) { /* handle the error condition */ }
    
}



- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    //Sound Buttons positioning
    if ([[UIScreen mainScreen] bounds].size.height < 568) {
        [soundbtn1 setFrame:CGRectMake(0, 130, 320, 50)];
        [soundbtn2 setFrame:CGRectMake(0, 203, 320, 50)];
        [soundbtn3 setFrame:CGRectMake(0, 276, 320, 50)];
        
    }
    
}





- (void)loadSleepSim{
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"sleep-easily-sleep-one1" ofType:@"mp3"];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SleepEasily_Short" ofType:@"mp3"];

    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if (error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }
    
    self.player.delegate = self;
    [self.player performSelector:@selector(play) withObject:nil afterDelay:0.5];
}


- (void)loadStillAwakeSound{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"StillAwakeLow" ofType:@"mp3"];
    
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if (error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }
    
    self.player.delegate = self;
    [self.player performSelector:@selector(play) withObject:nil afterDelay:0.5];
}



- (void)areYouAwake{
    NSLog(@"areYouAwake");
    [self pauseAction];
    
    self.imgView.image = [UIImage imageNamed:@"NewSleepFlow3.png"];
    
    [self loadStillAwakeSound];
    
    timer2 = [NSTimer scheduledTimerWithTimeInterval:(5) target:self selector:@selector(eyesClosedAction:) userInfo:nil repeats:NO];
//    timer2 = [NSTimer scheduledTimerWithTimeInterval:(60.0 * 10) target:self selector:@selector(eyesClosedAction:) userInfo:nil repeats:NO];
}



#pragma mark - IBAction Methods

- (IBAction)bedOutAction:(id)sender{
    [timer2 invalidate];
    
    
    //Log Sleep Action
    [[UserDefaultsStore shared] recordSleepAction:SleepAction_OutBed];
    
    
    //[self dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:^{
        //[[NSNotificationCenter defaultCenter] postNotificationName:kOutOfBedNotif object:nil];
    }];
}


- (IBAction)eyesClosedAction:(id)sender{
    [self pauseAction];

    
    self.imgView.image = [UIImage imageNamed:@"NewSleepScreen.png"];

    
    //Log Sleep Action
    [[UserDefaultsStore shared] recordSleepAction:SleepAction_EyesClosed];
    
    //Force Manual Sleep
    [[UserDefaultsStore shared] forceManualSleep];
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    EyesClosedViewController *eyesClosedVC = [storyboard instantiateViewControllerWithIdentifier:@"EyesClosedView"];
    [self.navigationController pushViewController:eyesClosedVC animated:NO];
}




- (IBAction)playPinkNoise:(id)sender{
    pauseImgView.hidden = NO;
    pauseButton.hidden = NO;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FanWhiteNoise_20Min_30SecFadeOut" ofType:@"mp3"];

    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if (error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }
    
    //self.player.delegate = self;
    [self.player performSelector:@selector(play) withObject:nil afterDelay:0.5];
    
    [timer1 invalidate];
    timer1 = [NSTimer scheduledTimerWithTimeInterval:(5) target:self selector:@selector(areYouAwake) userInfo:nil repeats:NO];
//    timer1 = [NSTimer scheduledTimerWithTimeInterval:(60.0 * 20) target:self selector:@selector(areYouAwake) userInfo:nil repeats:NO];

}

- (IBAction)playSleepSim:(id)sender{
    pauseImgView.hidden = NO;
    pauseButton.hidden = NO;
    
    [timer1 invalidate];
    timer1 = [NSTimer scheduledTimerWithTimeInterval:(60.0 * 20) target:self selector:@selector(areYouAwake) userInfo:nil repeats:NO];
    
    
    [self performSelector:@selector(loadSleepSim) withObject:nil afterDelay:0.5];
}

- (IBAction)playProgRelax:(id)sender{
    pauseImgView.hidden = NO;
    pauseButton.hidden = NO;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"progressiverelaxation" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSError *error = nil;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if (error) {
        NSLog(@"Error: %@", error.localizedDescription);
    }
    
    //self.player.delegate = self;
    [self.player performSelector:@selector(play) withObject:nil afterDelay:0.5];
    
    
    [timer1 invalidate];
    timer1 = [NSTimer scheduledTimerWithTimeInterval:(60.0 * 20) target:self selector:@selector(areYouAwake) userInfo:nil repeats:NO];

}

- (IBAction)playSilence:(id)sender{
    pauseImgView.hidden = NO;
    pauseButton.hidden = NO;
    
    
    [timer1 invalidate];
    timer1 = [NSTimer scheduledTimerWithTimeInterval:(60.0 * 20) target:self selector:@selector(areYouAwake) userInfo:nil repeats:NO];
}




- (IBAction)pauseAction{
    pauseImgView.hidden = YES;
    pauseButton.hidden = YES;
    [self.player stop];
    
    [timer1 invalidate];
}

@end
