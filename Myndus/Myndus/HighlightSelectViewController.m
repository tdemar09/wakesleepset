//
//  HighlightSelectViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/21/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "HighlightSelectViewController.h"
#import <AudioToolbox/AudioServices.h>
//#import "HighlightScoreViewController.h"
#import "UserDefaultsStore.h"


@interface HighlightSelectViewController ()
@property (nonatomic, strong) NSMutableArray *buttonArray, *selectedBtnArray, *connectionArray, *chosenWordsArray;
@property (nonatomic, strong) NSArray *highligherSentences;

@end

@implementation HighlightSelectViewController
@synthesize btnView;
@synthesize selectBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

}



- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.instrImageView.image = [UIImage imageNamed:@"HackIntro_Highlighter.png"];

    if (![[UserDefaultsStore shared] isHighlighterInstrDone]) {
        [[UserDefaultsStore shared] setHighlighterInstrDone:YES];
    }else {
        instrView.hidden = YES;
    }

    
    //self.navigationItem.hidesBackButton = YES;

    [self displaySentence];
}



- (void)displaySentence{
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"HighlighterHack" ofType:@"plist"];
    //self.highligherSentences = [[NSArray arrayWithContentsOfFile:path] mutableCopy];
    
    UserDefaultsStore *userDefaultsStore = [UserDefaultsStore shared];
    
    //NSString *sentence = [self.highligherSentences objectAtIndex:0];   //13
    NSString *sentence = [userDefaultsStore unAskedHighlighterSentence];
    NSLog(@"%@", sentence);
    
    
    CGFloat xLeft = 10, xRight = self.view.frame.size.width - 10;
    __block CGFloat x = xLeft;
    __block CGFloat y = 50;
    CGFloat spaceX = 15, spaceY = 45;
    
//    __block int count = 0;
    
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSArray *components = [sentence componentsSeparatedByCharactersInSet:charSet];
    self.buttonArray = [[NSMutableArray alloc] initWithCapacity:components.count];
    self.selectedBtnArray = [[NSMutableArray alloc] initWithCapacity:3];
    self.connectionArray = [[NSMutableArray alloc] initWithCapacity:3];

    
    
    //[sentence enumerateSubstringsInRange:NSMakeRange(0, sentence.length) options:NSStringEnumerationByWords usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
    [components enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
        NSString *substring = (NSString *)obj;
        //NSLog(@"Word:%@", substring);
        //NSString *test = @"of";
//        count++;
        
        
        
        //CGSize wordSize = [test sizeWithAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:25]}];
        CGSize wordSize = [self sizeForString:substring];
        //NSLog(@"Size: %f x %f", wordSize.width, wordSize.height);
        
        
        
        //Set new coordinates
        if (x >= xLeft) {
            CGFloat tempX = x + wordSize.width;
            
            //check if space can be used
            if (tempX + spaceX < xRight) {
                x = x + spaceX;
            }
            
            //check without space
            else if (tempX + 2 <= xRight) {
                x = x + 7;
            }
            
            //go to next line
            else {
                x = xLeft;
                y = y + wordSize.height + spaceY;
            }
        }
        
        
        //Set Button up
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setFrame:CGRectMake(x, y, wordSize.width, wordSize.height)];
        [btn setTitle:substring forState:UIControlStateNormal];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:25]];
        [btn setBackgroundColor:[UIColor colorWithRed:94/255.0 green:171/255.0 blue:222/255.0 alpha:1.0]];
        [btn.layer setCornerRadius:6.0f];
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //Add button to array
        [self.buttonArray addObject:btn];
        
        //Add button to view
        [btnView addSubview:btn];
        
        
        //set current x
        x = x+wordSize.width;
        
        
        
//        if (count > 3)
//            //*stop = YES;
        
    }];
}


- (CGSize)sizeForString:(NSString *)string{
    CGSize wordSize = [string sizeWithAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName: [UIFont systemFontOfSize:25]}];

    CGFloat width = wordSize.width+3, height = wordSize.height;
    //NSLog(@"sizeForString: %f x %f", wordSize.width, wordSize.height);

    
    if (wordSize.width < 40.0f) {
        width = 40.0;
    }
    if (wordSize.height < 30.0f) {
        height = 30.0;
    }
    
    return CGSizeMake(width, height);
}


- (void)addConnectionToButton:(UIButton *)postBtn{
    
    //2nd and up button
    if (self.selectedBtnArray.count > 0) {
        UIButton *preBtn = [self.selectedBtnArray objectAtIndex:self.selectedBtnArray.count-1];
        
        //find PRE center
        CGPoint preCenter = CGPointMake(preBtn.frame.origin.x + (preBtn.frame.size.width/2), preBtn.frame.origin.y + (preBtn.frame.size.height/2));
        
        
        //find POST center
        CGPoint postCenter = CGPointMake(postBtn.frame.origin.x + (postBtn.frame.size.width/2), postBtn.frame.origin.y + (postBtn.frame.size.height/2));

        //Create path
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:preCenter];
        [path addLineToPoint:postCenter];
        
        //create shapelayer
        CAShapeLayer *shapeLayer = [CAShapeLayer layer];
        shapeLayer.path = [path CGPath];
        shapeLayer.strokeColor = [[UIColor colorWithRed:255/255.0 green:93/255.0 blue:130/255.0 alpha:1.0] CGColor];
        shapeLayer.lineWidth = 3.0;
        shapeLayer.fillColor = [[UIColor clearColor] CGColor];
        
        
        //add shapelayer
        [self.connectionArray addObject:shapeLayer];
        [self.view.layer insertSublayer:shapeLayer below:self.btnView.layer];
    }
}


- (void)removeConnectionToButton:(UIButton *)postBtn{
    if (self.connectionArray.count > 0) {
//        //Deselected button is not the last button selected
//        if (postBtn != [self.selectedBtnArray objectAtIndex:self.selectedBtnArray.count-1]) {
//            //remove connection from here down
//            
//        }
//        else{
//            //only remove this connection
//            
//        }
        
        CAShapeLayer *shapeLayer = [self.connectionArray objectAtIndex:self.connectionArray.count-1];
        [shapeLayer removeFromSuperlayer];
        [self.connectionArray removeLastObject];

    }
    
}


- (void)vibrate{
    AudioServicesPlaySystemSound (1352);
}



#pragma mark - IBAction Methods

- (IBAction)btnAction:(id)sender{
    UIButton *btn = (UIButton *)sender;
    //NSLog(@"Clicked: %@", btn.titleLabel.text);
    
    
    //Set All to Gray on 1st Click
    if (self.selectedBtnArray.count == 0) {
        for (UIButton *button in self.buttonArray) {
            [button setBackgroundColor:[UIColor lightGrayColor]];
        }
    }
    
    
    //Color Select
    if (!btn.selected) {
        //Disable if 3 are already clicked
        if (self.selectedBtnArray.count == 3)
            return;
        
        [btn setSelected:YES];
        [btn setBackgroundColor:[UIColor colorWithRed:255/255.0 green:93/255.0 blue:130/255.0 alpha:1.0]];
        [self addConnectionToButton:btn];
        [self.selectedBtnArray addObject:btn];

        [self vibrate];
    }
    
    //Color De-Select
    else{
        
        if (btn != [self.selectedBtnArray lastObject]) {
            //remove button from here down (in reverse order)
            NSUInteger index = [self.selectedBtnArray indexOfObject:btn];
            [self.selectedBtnArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

                UIButton *removeBtn = (UIButton *)obj;
                [removeBtn setSelected:NO];
                [removeBtn setBackgroundColor:[UIColor lightGrayColor]];
                
                [self removeConnectionToButton:removeBtn];
                [self.selectedBtnArray removeObject:removeBtn];
                
                if (idx == index)
                    *stop = YES;
            }];
            
        }
        else {
            //only remove this connection
            [btn setSelected:NO];
            [btn setBackgroundColor:[UIColor lightGrayColor]];
            
            [self removeConnectionToButton:btn];
            [self.selectedBtnArray removeObject:btn];
        }
             
        
        
    }
    
    
    //select button text
    switch (self.selectedBtnArray.count) {
        case 0:
            [selectBtn setTitle:@"Select 3" forState:UIControlStateNormal];
            selectBtn.layer.borderWidth = 0;
            selectBtn.enabled = NO;
            break;
            
        case 1:
            [selectBtn setTitle:@"Select 2" forState:UIControlStateNormal];
            selectBtn.layer.borderWidth = 0;
            selectBtn.enabled = NO;
            break;
            
        case 2:
            [selectBtn setTitle:@"Select 1" forState:UIControlStateNormal];
            selectBtn.layer.borderWidth = 0;
            selectBtn.enabled = NO;
            break;
            
        case 3:
            [selectBtn setTitle:@"Done" forState:UIControlStateNormal];
            selectBtn.layer.borderColor = [UIColor blackColor].CGColor;
            selectBtn.layer.borderWidth = 2;
            selectBtn.enabled = YES;
            break;
            
        default:
            break;
    }
}

- (IBAction)continueAction:(id)sender{
    
    self.chosenWordsArray = [[NSMutableArray alloc] initWithCapacity:3];
    for (UIButton *button in self.selectedBtnArray) {
        [self.chosenWordsArray addObject:button.titleLabel.text];
    }
    NSLog(@"Selected: %@", self.chosenWordsArray);

    [[UserDefaultsStore shared] highlighterHackChoseWords:self.chosenWordsArray];
}



@end
