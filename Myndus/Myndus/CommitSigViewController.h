//
//  CommitSigViewController.h
//  Mynd
//
//  Created by Troy DeMar on 7/23/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommitSigViewController.h"
#import "HackBaseViewController.h"

@interface CommitSigViewController : HackBaseViewController


@property (nonatomic, weak) CommitSigViewController *containerSigViewController;

@end
