//
//  HackBaseViewController.m
//  Myndus
//
//  Created by Troy DeMar on 10/1/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "HackBaseViewController.h"
#import "AppDelegate.h"
#import "SleepRitualGameViewController.h"
#import "CommitSelectViewController.h"
#import "HighlightSelectViewController.h"
#import "HackPersonalityViewController.h"

@interface HackBaseViewController ()
@property (nonatomic) BOOL showingModal;
@end

@implementation HackBaseViewController
@synthesize instrView, instrImageView, instrBtn;
@synthesize navSlider;
@synthesize sequence, repeatSeq;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.instrView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [self.view addSubview:instrView];
    
    //Instruction ImageView
    instrImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    [instrView addSubview:instrImageView];
    
    //Instruction Play Button
    instrBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [instrBtn setFrame:CGRectMake(15, 510, 292, 58)];
    [instrBtn addTarget:self action:@selector(instrPlayAction:) forControlEvents:UIControlEventTouchUpInside];
    //[btn setTitle:@"Play" forState:UIControlStateNormal];
    //[btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[btn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [instrView addSubview:instrBtn];
    
    
    //Nav Bar View
    //self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    self.navView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.navView];
    
    
    //UIButton *menuBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    UIButton *menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuBtn setFrame:CGRectMake(5, 0, 40, 40)];
    //[menuBtn setBackgroundImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    menuBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    //[menuBtn.imageView setImage:[UIImage imageNamed:@"menu"]];
    [menuBtn setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];

    //menuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    //menuBtn.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    [menuBtn addTarget:self action:@selector(menuAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:menuBtn];
    
    
    
    UIButton *quesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [quesBtn setFrame:CGRectMake(50, 5, 35, 30)];
    [quesBtn setImage:[UIImage imageNamed:@"QuestionMark.png"] forState:UIControlStateNormal];
    quesBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    quesBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    quesBtn.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    [quesBtn addTarget:self action:@selector(showInstrAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.navView addSubview:quesBtn];

    

    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    navSlider = [delegate mainSlider];
    //navSlider = [delegate newSlider];
    navSlider.value = [delegate.sliderNumber floatValue];
    NSLog(@"SLIDER VALUE: %f", delegate.sliderNumber.floatValue);
    //[navSlider addTarget:self action:@selector(sliderChangeAction:) forControlEvents:UIControlEventValueChanged];

    [self.navView addSubview:navSlider];

}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.navView addSubview:navSlider];
}



- (void)updateBaseLayouts{
    NSLog(@"Base: updateBaseLayouts");
    //[self.view insertSubview:self.instrView atIndex:0];
    //[self.view insertSubview:self.navView atIndex:1];
    
    [self.view addSubview:self.instrView];
    [self.view addSubview:self.navView];
}


#pragma mark - IBAction Methods

- (IBAction)instrPlayAction:(id)sender{
    self.instrView.hidden = YES;
    
}

- (IBAction)repeatSequence:(id)sender {
    //[self.navigationController popToRootViewControllerAnimated:NO];
    
    //Random Commit / Highlight / Personality
    NSInteger min = 1;
    NSInteger max = 3;
    NSInteger random = min + arc4random_uniform(max - min + 1);
    
    NSLog(@"Random: %i", random);
    if (random == 1) {
        //Commitment
        CommitSelectViewController *commitController = [self.storyboard instantiateViewControllerWithIdentifier:@"SeqNew_CommitSelectController"];
        [self.navigationController pushViewController:commitController animated:YES];
    }
    
    else if (random == 2) {
        //Highlighter
        HighlightSelectViewController *highlightController = [self.storyboard instantiateViewControllerWithIdentifier:@"SeqNew_HighlightSelectController"];
        [self.navigationController pushViewController:highlightController animated:YES];
    }
    
    else {
        //Personality
        HackPersonalityViewController *personalityController = [self.storyboard instantiateViewControllerWithIdentifier:@"SeqNew_HackPersonalityController"];
        [self.navigationController pushViewController:personalityController animated:YES];
    }
}


- (IBAction)menuAction:(id)sender{
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Mynd Menu" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Let's wake up", @"Let's get sleepy", @"Let's go to sleep", @"Commitments", nil];
    [sheet showInView:self.view];
}

- (IBAction)showInstrAction:(id)sender{
    NSLog(@"showInstrAction");
    self.instrView.hidden = NO;
}


- (IBAction)sliderChangeAction:(id)sender{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate setSliderNumber:[NSNumber numberWithFloat:navSlider.value]];
    
}



#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return NO;
}



#pragma mark - UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%i", buttonIndex);
    //[self dismissViewControllerAnimated:NO completion:nil];
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    
    if (buttonIndex == 0) {
        //InterventionWake
        [delegate presentSequence:HackSequenceInterventionWake];
    }
    
    else if (buttonIndex == 1) {
        //InterventionSleep
        [delegate presentSequence:HackSequenceInterventionSleep];
    }
    

    else if (buttonIndex == 2) {
        //Sleep
        [delegate presentSequence:HackSequenceSleep];
    }
    
    
    else if (buttonIndex == 3) {
        //Commitments
        [delegate presentSequence:HackSequenceCommitList];
    }
    
    
    else {
        //Nothing
        
    }
}





@end
