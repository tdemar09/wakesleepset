//
//  EyesClosedViewController.m
//  Mynd
//
//  Created by Troy DeMar on 7/20/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "EyesClosedViewController.h"
#import "UserDefaultsStore.h"
//#import "Indicative.h"
//#import "MMPDeepSleepPreventer.h"
//#import <CoreMotion/CoreMotion.h>
@interface EyesClosedViewController (){
//    MMPDeepSleepPreventer *Obj_DeepSleepPreventer;
}
@end

@implementation EyesClosedViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidLoad{
    [super viewDidLoad];
    
    
}



#pragma mark - IBAction Methods

- (IBAction)eyesOpenAction:(id)sender{
    
    [[UserDefaultsStore shared] recordSleepAction:SleepAction_EyesOpened];
    
    //[self.navigationController popToRootViewControllerAnimated:NO];
    [self.navigationController popViewControllerAnimated:NO];
}

@end
