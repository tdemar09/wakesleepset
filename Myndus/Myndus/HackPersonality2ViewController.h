//
//  HackPersonality2ViewController.h
//  Mynd
//
//  Created by Dhruv on 19/08/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HackBaseViewController.h"
#import "NSDate-Utilities.h"

@interface HackPersonality2ViewController : HackBaseViewController
{
    IBOutlet UIButton *btn_Continue;
    
    IBOutlet UIButton *btn_1_Yes;
    IBOutlet UIButton *btn_2_Yes;
    IBOutlet UIButton *btn_3_Yes;
    IBOutlet UIButton *btn_4_Yes;
    IBOutlet UIButton *btn_5_Yes;
    IBOutlet UIButton *btn_1_No;
    IBOutlet UIButton *btn_2_No;
    IBOutlet UIButton *btn_3_No;
    IBOutlet UIButton *btn_4_No;
    IBOutlet UIButton *btn_5_No;
    IBOutlet UILabel *lbl_PersonQ1;
    IBOutlet UILabel *lbl_PersonQ2;
    IBOutlet UILabel *lbl_PersonQ3;
    IBOutlet UILabel *lbl_PersonQ4;
    IBOutlet UILabel *lbl_PersonQ5;
}
@property(nonatomic,weak) NSArray *arr_SecondFiveQ;
@end
