//
//  RotateHackViewController.h
//  Myndus
//
//  Created by Troy DeMar on 10/2/14.
//  Copyright (c) 2014 Troy. All rights reserved.
//

#import "HackBaseViewController.h"

@interface RotateHackViewController : HackBaseViewController {
    IBOutlet UIImageView *imgView;
}

@property (strong, nonatomic) UIImageView *imgView;
@property (nonatomic) NSInteger count;

@end
