//
//  CircleNode.h
//  Mynd
//
//  Created by Troy DeMar on 8/14/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CircleNode;
@protocol CircleNodeDelegate <NSObject>
- (void)touchedDownCircleNode:(CircleNode *)node;
- (void)touchedUpCircleNode:(CircleNode *)node;

@end


@interface CircleNode : UIView {
    UIImageView *imgView;
    
    CircleNode *top, *left, *right, *bottom;
    CircleNode *topLeft, *topRight, *bottomLeft, *bottomRight;
    int x, y;
    BOOL isTail;
}

@property (strong, nonatomic) CircleNode *top, *left, *right, *bottom;
@property (strong, nonatomic) CircleNode *topLeft, *topRight, *bottomLeft, *bottomRight;
@property (nonatomic, weak) id <CircleNodeDelegate> delegate;
@property (nonatomic) int x, y;
@property (nonatomic) HackSequence seq;

- (void)colorNormal;
- (void)colorMain;
- (void)colorTail1;
- (void)colorTail2;
- (BOOL)isColoredTail;

- (void)highlightAllConnections:(BOOL)on;


- (void)flickOn:(BOOL)on;
- (void)highlightConnectedOn:(BOOL)on;

@end
