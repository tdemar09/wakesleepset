//
//  RatePersonalityViewController.m
//  Mynd
//
//  Created by Dhruv on 19/08/14.
//  Copyright (c) 2014 Appurava. All rights reserved.
//

#import "RatePersonalityViewController.h"

@interface RatePersonalityViewController (){

}
@end

@implementation RatePersonalityViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.936 green:0.418 blue:0.155 alpha:1.000] slider:sld_mnd_emo withImageForThumb:[UIImage imageNamed:@"Orange.png"]];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.936 green:0.418 blue:0.155 alpha:1.000] slider:sld_mnd_lol withImageForThumb:[UIImage imageNamed:@"Orange.png"]];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.936 green:0.418 blue:0.155 alpha:1.000] slider:sld_mnd_spMs withImageForThumb:[UIImage imageNamed:@"Orange.png"]];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.936 green:0.418 blue:0.155 alpha:1.000] slider:sld_mnd_swr withImageForThumb:[UIImage imageNamed:@"Orange.png"]];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.547 green:0.849 blue:0.994 alpha:1.000] slider:sld_do_emo withImageForThumb:[UIImage imageNamed:@"BlueCircle.png"]];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.547 green:0.849 blue:0.994 alpha:1.000] slider:sld_do_lol withImageForThumb:[UIImage imageNamed:@"BlueCircle.png"]];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.547 green:0.849 blue:0.994 alpha:1.000] slider:sld_do_spMs withImageForThumb:[UIImage imageNamed:@"BlueCircle.png"]];
    [self SetAllSliderForChangesInColor:[UIColor colorWithRed:0.547 green:0.849 blue:0.994 alpha:1.000] slider:sld_do_swr withImageForThumb:[UIImage imageNamed:@"BlueCircle.png"]];
    
}
-(void)SetAllSliderForChangesInColor:(UIColor *)color slider:(UISlider *)slider withImageForThumb:(UIImage *)img
{
    [slider setThumbImage:[img stretchableImageWithLeftCapWidth:4 topCapHeight:0] forState:UIControlStateNormal];
    [slider setMaximumTrackTintColor:[UIColor blackColor]];
    [slider setMinimumTrackTintColor:[UIColor blackColor]];
    [slider setThumbTintColor:color];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)ClickedContinue:(id)sender
{
    if ([self networkCheck])
    {
            SendMail *sendMail = [SendMail new];
            [sendMail sendMessageInBack:[NSString stringWithFormat:@"1-Do you mind?\n2-Do you do? \nSwearing1-->%d\nSwearing2-->%d\nSpelling Mistakes1-->%d\nSpelling Mistakes2-->%d\nEmoticons1-->%d\nEmoticons2-->%d\nlol, wtf, etc1-->%d\nlol, wtf, etc2-->%d",(int)roundf(sld_mnd_swr.value),(int)roundf(sld_do_swr.value),(int)roundf(sld_mnd_spMs.value),(int)roundf(sld_do_spMs.value),(int)roundf(sld_mnd_emo.value),(int)roundf(sld_do_emo.value),(int)roundf(sld_mnd_lol.value),(int)roundf(sld_do_lol.value)]withDelegate:self withTag:nil];
             [self performSegueWithIdentifier:@"ToLastScore" sender:nil];
            return ;
    }
}
-(IBAction)EyeClick:(id)sender
{
    self.navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (BOOL)networkCheck {
    
    Reachability *wifiReach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            NSLog(@"NETWORKCHECK: Not Connected");
            return NO;
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"NETWORKCHECK: Connected Via WWAN");
            return YES;
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"NETWORKCHECK: Connected Via WiFi");
            return YES;
            break;
        }
    }
    return false;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
